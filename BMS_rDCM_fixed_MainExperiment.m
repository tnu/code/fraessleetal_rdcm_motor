function BMS_rDCM_fixed_MainExperiment(subject_analyze,aspirin,parcellation_type)
% Compare different whole-brain Dynamic Causal Models (DCMs) for the 
% hand movement dataset. Specifically, the function compares models with 
% different fixed endogenous connectivity patterns, including an anatomically 
% informed model based on the Brainnetome atlas and two control models.
% 
% Input:
%   subject_analyze     -- subjects to include (default: empty)
%   aspirin             -- (0) no aspirin and (1) aspirin
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainnetome 2016, (4) Glasser 2016 & Buckner 2011
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the experiment folders
aspirin_folder    = {'wo_Aspirin/','Aspirin/','all_Aspirin/'};
handedness_folder = {'left','right'};

% add the relevant paths and set the foldername
addpath(fullfile(FilenameInfo.SPM_path))
exp_foldername  = FilenameInfo.DataPath;


% define the different names
scale_name      = {'_noScale',''};

% set the parcellation
parcellations = {'Glasser2016','AAL','Brainnetome2016','Glasser2016_Buckner2011'};

% set to no scaling
scale = 0;

% close all figures
close all


% display progress
disp('Loading data...')


% initialize the 
F_allSubjects = cell(length(handedness_folder),1);


for handedness = 1:length(handedness_folder)
    if ( ~isempty(aspirin) )

        % set the SPM path and the path to the experiment
        foldername = [exp_foldername aspirin_folder{aspirin+1}];

        % iterate over models
        for model = 1:3

            % load the results file
            temp = load(fullfile(foldername,'DCM_GroupAnalysis',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome'], ['PosteriorParameterEstimates_model' num2str(model) '.mat']));

            % asign the data
            F_allSubjects{handedness}(:,model) = temp.results.F_AllSubjects{handedness};

        end

    else

        % iterate over models
        for model = 1:3

            % iterate over both conditions
            for aspirin_ind = [0 1]

                % set the SPM path and the path to the experiment
                foldername = [exp_foldername aspirin_folder{aspirin_ind+1}];

                % load the results file
                temp = load(fullfile(foldername,'DCM_GroupAnalysis',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome'], ['PosteriorParameterEstimates_model' num2str(model) '.mat']));

                % asign the data
                if ( aspirin_ind == 0 )
                    F_allSubjects_temp = temp.results.F_AllSubjects{handedness};
                else
                    F_allSubjects_temp = [F_allSubjects_temp; temp.results.F_AllSubjects{handedness}];
                end
            end
            
            % asign the negative free energies for all subjects
            F_allSubjects{handedness}(:,model) = F_allSubjects_temp;
            
        end
    end
end


% set aspirin condition to all subjects
aspirin = 2;


% specify an indicator vector
if ( isempty(subject_analyze) )
    if ( aspirin == 0 )
        vector = [1:7 10:size(F_allSubjects{1},1)];
    elseif ( aspirin == 1 )
        vector = [1:4 7:size(F_allSubjects{1},1)];
    elseif ( aspirin == 2 )
        vector = [[1:7 10:14] [1:4 7:15]+14];
    end
else
    vector = subject_analyze;
end


% sanity check: any infinite or NaN values
for handedness = 1:2
    if ( any(~isfinite(F_allSubjects{handedness}(vector,:))) )
        disp(['NaN value detected for condition: ' handedness_folder{handedness} ' hand'])
        return
    end
end


% sum over sessions (left + right)
F_allSubjects_allSessions = F_allSubjects{1} + F_allSubjects{2};


% dsiplay progress
disp(['# Subjects found: ' num2str(length(vector))])


% asign the negative free energy array
F = F_allSubjects_allSessions(vector,:);

% M: number of models
M = size(F,2);

% call the random effects BMS function
[~, exp_r, ~, pxp, ~] = spm_BMS(F, 1e6, 1, 0, 1, ones(1,M));


% close all open figures
close all

% define the names
model_names = {'Brainnetome','Random','Full'};


% specify the folder where results should be stored
results_folder = fullfile(exp_foldername,'Figures','IBS',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome'],'Hand',aspirin_folder{aspirin+1});

% create the directory
if ( ~exist(results_folder,'dir') ) 
    mkdir(results_folder)
else
    if ( exist(fullfile(results_folder,'AllModels_BMS_rDCM.ps'),'file') )
        delete(fullfile(results_folder,'AllModels_BMS_rDCM.ps'))
    end
end


% plot the expected probability
figure('units','normalized','outerposition',[0 0 1 1]);
col = [0.7 0.7 0.7];
bar(exp_r,'FaceColor',col);
xlim([0.25 size(pxp,2)+0.75])
title('BMS: Benefit of anatomical information','FontSize',20)
xlabel('Model / Connectivity','FontSize',18)
ylabel('expected posterior probability (exp)','FontSize',18)
set(gca,'FontSize',18);
set(gca,'ytick',[0 0.5 1])
set(gca,'xtick',1:3)
set(gca,'xticklabel',model_names)
box off
axis square
print(gcf, '-dpsc', fullfile(results_folder,'AllModels_BMS_rDCM'), '-append')

% plot the protected exceedance probability
figure('units','normalized','outerposition',[0 0 1 1]);
col = [0.7 0.7 0.7];
bar(pxp,'FaceColor',col);
xlim([0.25 size(pxp,2)+0.75])
title('BMS: Benefit of anatomical information','FontSize',20)
xlabel('Model / Connectivity','FontSize',18)
ylabel('protected exceedance probability (pxp)','FontSize',18)
set(gca,'FontSize',18);
set(gca,'ytick',[0 0.5 1])
set(gca,'xtick',1:3)
set(gca,'xticklabel',model_names)
box off
axis square
print(gcf, '-dpsc', fullfile(results_folder,'AllModels_BMS_rDCM'), '-append')

end