function get_VOI_coordinates_MainExperiment(parcellation_type)
% Collects the center coordinate of the regions of interest (ROIs) from
% each participant of the hand movement dataset.
% 
% Input:
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainnetome 2016, (4) Glasser 2016 & Buckner 2011
% 
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end

% define important folders
aspirin_folder      = {'wo_Aspirin/','Aspirin/'};
handedness_folder   = {'left','right'};

% set the different parcellation options
parcellations = {'Glasser2016','AAL','Brainnetome2016','Glasser2016_Buckner2011'};

% specify the hand movement
aspirin_analyze    = 1:length(aspirin_folder);
handedness_analyze = 1:length(handedness_folder);


% get the mask information
mask_info = create_mask_information_MainExperiment(parcellation_type);


% specify the coordinate array
center_allregions = cell(12,size(mask_info,1),length(aspirin_analyze),length(handedness_analyze));

% specify cell for incomplete subjects
dimension_mismatch = cell(length(aspirin_analyze),length(handedness_analyze),1);


% iterate over aspirin conditions
for aspirin = aspirin_analyze
    
    % set the SPM path
    addpath(FilenameInfo.SPM_path);
    
    
    % set the path to the experiment
    foldername	= fullfile(FilenameInfo.DataPath, aspirin_folder{aspirin});
    VoI_name  	= FilenameInfo.DataPath;
    
    
    % initialize spm
    spm_jobman('initcfg')

    % get the subject list
    Subject_List = dir(fullfile(foldername, '1*'));
    
    % iterate over all settings
    for handedness = handedness_analyze
        
        % define the subjects to analyze
        subjects_analyze = 1:length(Subject_List);

        for s = subjects_analyze

            % subject name
            Subject = Subject_List(s).name;
            
            % display the progress
            disp(['Processing: ' aspirin_folder{aspirin} ' - ' Subject ' - ' handedness_folder{handedness}])

            % specify the SPM folder
            SPM_foldername = fullfile(foldername,Subject,'FirstLevel','IBS',['tapping_' handedness_folder{handedness}]);

            % check if subject has data
            files = dir(fullfile(SPM_foldername,'SPM.mat'));
            run_subject = ~isempty(files);

            % perform BOLD signal time series extraction
            if ( run_subject )

                % time series folder
                timeseries_folder = fullfile(SPM_foldername,'timeseries_VOI_adjusted',parcellations{parcellation_type});
                
                % extract the time series for each region of interest
                for number_of_regions = 1:size(mask_info,1)

                    % load the VOI to get the coordinates
                    if ( exist(fullfile(timeseries_folder,['VOI_' mask_info{number_of_regions,1} '_1.mat']),'file') )

                        % load the VOI
                        load(fullfile(timeseries_folder,['VOI_' mask_info{number_of_regions,1} '_1.mat']))

                        % get the center coordinates for the extracted time series
                        center_allregions{s,number_of_regions,aspirin,handedness} = xY.xyz';

                    else

                        % set the coordinate to NaN
                        center_allregions{s,number_of_regions,aspirin,handedness} = NaN;

                        % store the subject as not completed
                        if ( isempty(dimension_mismatch{aspirin,handedness,1}) )
                            dimension_mismatch{aspirin,handedness,1} = Subject;
                        else
                            dimension_mismatch{aspirin,handedness,end+1} = Subject;
                        end
                    end
                end
            end
        end
    end
end


% foldername for the center coordinates
VoI_foldername = fullfile(VoI_name,'VoI_coordinates','IBS',parcellations{parcellation_type});

% create the VoI folder
if ( ~exist(VoI_foldername,'dir') )
    mkdir(VoI_foldername)
end

% save the center coordinates in a file
save(fullfile(VoI_foldername,'VoI_CenterCoordinates_AllSubjects.mat'),'center_allregions','dimension_mismatch')

end
