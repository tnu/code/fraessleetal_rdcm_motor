function estimate_FC_MainExperiment(subject,aspirin,handedness,parcellation_type,computeGT)
% Infer whole-brain functional connectivity for the Hand Movement dataset
% of the Stroke study using full correlation and partial correlation.
% 
% This function prepares the respective DCM model. Futher, it applies the
% functional connectivity schemes.
% 
% Input:
%   subject             -- subject to be analyzed
%   aspirin             -- (0) no aspirin and (1) aspirin
% 	handedness         	-- left- or right-hand movement dataset
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainetome 2016, (4) Glasser 2016 & Buckner 2011
%   computeGT           -- (0) don't or (1) compute graph theoretical measures
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the experiment folders
aspirin_folder    = {'wo_Aspirin/','Aspirin/'};
handedness_folder = {'left','right'};


% add the relevant paths and set the foldername
pre_foldername = FilenameInfo.DataPath;
addpath(fullfile(FilenameInfo.MATLAB_path,'FunctionalConnectivityToolbox'))
addpath(FilenameInfo.SPM_path);


% define the different names
foldername      = fullfile(pre_foldername, aspirin_folder{aspirin+1});


% get the subject list
Subject_List = dir(fullfile(foldername,'1*'));

% subjects to analyze
if ( isempty(subject) )
    subject_analyze = 1:length(Subject_List);
else
    subject_analyze = subject;
end


% iterate over subjects
for subject = 1:length(Subject_List)
    
    % subject name
    Subject = Subject_List(subject).name;

    % display subject name
    disp(['Subject: ' Subject])
    
    % check whether subject should be included
    if ( any(subject==subject_analyze) )

        % clear any DCM structure
        clear DCM

        % set the parcellation
        parcellations     = {'Glasser2016','AAL','Brainnetome2016','Glasser2016_Buckner2011'};

        % load the SPM
        load(fullfile(foldername, Subject, 'FirstLevel', 'IBS', ['tapping_' handedness_folder{handedness}], 'SPM.mat'))

        % asign the data of the respective subject to the DCM
        VOI_foldername = fullfile(foldername, Subject, 'FirstLevel', 'IBS', ['tapping_' handedness_folder{handedness}], 'timeseries_VOI_adjusted', parcellations{parcellation_type});


        % get the ROI names that are not present in all subjects
        temp_ROI = load(fullfile(pre_foldername,'VoI_coordinates','IBS',parcellations{parcellation_type},'MissingROIs.mat'));

        % remove ROIs that are not present in all subjects
        for ROI_int = 1:length(temp_ROI.allMissing_regions)
            delete(fullfile(VOI_foldername,['VOI_*' temp_ROI.allMissing_regions{ROI_int}(5:(find(temp_ROI.allMissing_regions{ROI_int}=='_',1,'last'))) '*.mat']))
        end


        % get the VOI file in the folder
        VOI_files = dir(fullfile(VOI_foldername, 'VOI*.mat'));

        % display progress
        disp('Loading data...')
        disp(['Found number of regions: ' num2str(length(VOI_files))])

        % load the VOI time series for all regions of interest
        for number_of_regions = 1:length(VOI_files)

            % load the files
            load(fullfile(VOI_foldername,VOI_files(number_of_regions).name),'xY');
            DCM.xY(number_of_regions) = xY;

        end

        % number of regions
        DCM.n = length(DCM.xY);

        % number of time points
        DCM.v = length(DCM.xY(1).u);

        % specify the TR
        DCM.Y.dt  = SPM.xY.RT;

        % specify the Y component of the DCM file
        DCM.Y.X0 = DCM.xY(1).X0;

        % asign the data to the Y structure
        for i = 1:DCM.n
            DCM.Y.y(:,i)  = DCM.xY(i).u;
            DCM.Y.name{i} = DCM.xY(i).name;
        end

        % define the covariance matrix
        DCM.Y.Q = spm_Ce(ones(1,DCM.n)*DCM.v);


        % display the progress
        disp(['Evaluating subject ' num2str(subject)])
        disp(' ')


        % evaluate full (Pearson) correlation
        disp('Full correlation...')
        fullCorr = corrcoef(DCM.Y.y);

        % evaluate partial correlation
        disp('Partial correlation...')
        partialCorr = partialcorr(DCM.Y.y);
        
    else
        
        % display the progress
        disp(['Evaluating subject ' num2str(subject) ' (not included)'])
        
        % empty FC measures
        fullCorr    = NaN(size(fullCorr));
        partialCorr = NaN(size(partialCorr));
        
    end
    
    % line breaks
    fprintf('\n\n')
    
    % asign the functional connectivity measures
    FC_measures.fullCorr        = fullCorr;
    FC_measures.partialCorr     = partialCorr;
    
    
    % specify the results array for all subjects
    if ( subject == 1 )
        fullCorr_AllSubjects    = cell(size(fullCorr));
        partialCorr_AllSubjects = cell(size(partialCorr));
    end

    % asign the a values for the endogenous parameters in each subject
    for int = 1:size(fullCorr,1)
        for int2 = 1:size(fullCorr,2)
            fullCorr_AllSubjects{int,int2}      = [fullCorr_AllSubjects{int,int2}; fullCorr(int,int2)];
            partialCorr_AllSubjects{int,int2}	= [partialCorr_AllSubjects{int,int2}; partialCorr(int,int2)];
        end 
    end
end


% asign the results
FC_measures.fullCorr_AllSubjects    = fullCorr_AllSubjects;
FC_measures.partialCorr_AllSubjects = partialCorr_AllSubjects;


% store the functional connectivity results
if ( ~exist(fullfile(foldername,'FC_GroupAnalysis',['tapping_' handedness_folder{handedness}],parcellations{parcellation_type}),'dir') )
    mkdir(fullfile(foldername,'FC_GroupAnalysis',['tapping_' handedness_folder{handedness}],parcellations{parcellation_type}) )
end
save(fullfile(foldername,'FC_GroupAnalysis',['tapping_' handedness_folder{handedness}],parcellations{parcellation_type},'FC_measures.mat'),'FC_measures')


% compute graph theory measures
if ( computeGT )
    
    fprintf('\nComputing graph theoretical measures...\n')
    
    % compute graph theoretical measures
    for s = 1:size(fullCorr_AllSubjects{1,1},1)
        
        % get the subject specific data again
        fullCorr_subject = NaN(size(fullCorr_AllSubjects,1),size(fullCorr_AllSubjects,1));
        for int = 1:size(fullCorr_AllSubjects,1)
            for int2 = 1:size(fullCorr_AllSubjects,2)
                fullCorr_subject(int,int2) = fullCorr_AllSubjects{int,int2}(s);
            end
        end
        
        % remove the diagonal
        fullCorr_temp = fullCorr_subject-diag(diag(fullCorr_subject));
        
        % transpose FC matrix (as Brain Connectivity toolbox uses switched notation)
        fullCorr_temp = fullCorr_temp';
        
        % compute graph theoretical measures
        if ( any(fullCorr_temp(:)~=0) && any(isfinite(fullCorr_temp(:))) )
            disp(['Subject #: ' num2str(s)])
            results_temp = compute_graph_theoretical_measures_undirected(fullCorr_temp);
            results(s)   = results_temp;
        else
            disp(['Subject #: ' num2str(s) ' (not included)'])
        end
    end
    
    % create the results folder
    if ( ~exist(fullfile(foldername,'FC_GroupAnalysis',['tapping_' handedness_folder{handedness}],parcellations{parcellation_type}),'dir') )
        mkdir(fullfile(foldername,'FC_GroupAnalysis',['tapping_' handedness_folder{handedness}],parcellations{parcellation_type}))
    end
    
    % store the graph theoretical measures
    if ( ~isempty(results) )
        save(fullfile(foldername,'FC_GroupAnalysis',['tapping_' handedness_folder{handedness}],parcellations{parcellation_type},'FC_GraphTheory.mat'), 'results')
    end
end

end