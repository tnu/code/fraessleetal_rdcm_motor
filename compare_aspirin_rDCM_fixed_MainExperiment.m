function compare_aspirin_rDCM_fixed_MainExperiment(model,parcellation_type,p_corr)
% Compare parameter estimates from the whole-brain Dynamic Causal Models 
% (DCMs) for the aspirin and without aspirin healthy control of the Stroke patient 
% study. Parameter estimates have been computed using regression DCM (rDCM)
% under an anatomically informed network structure.
% 
% This function reads the summary file containing all the DCM parameter 
% estimates that have been estimated using regression DCM. The function 
% then evaluates the difference between aspirin and no-aspirin.
% 
% Input:
%   model               -- model to analyze
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainnetome 2016, (4) Glasser 2016 & Buckner 2011
%   p_corr              -- mutliple comparison correction: (1) FDR, (0) uncorrected
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the experiment folders
aspirin_folder    = {'wo_Aspirin/','Aspirin/'};

% set the foldername
exp_foldername = FilenameInfo.DataPath;
addpath(fullfile(FilenameInfo.MATLAB_path,'fdr_bh'))
addpath(fullfile(FilenameInfo.MATLAB_path,'bipolar_colormap'))
addpath(fullfile(FilenameInfo.MATLAB_path,'simple_mixed_anova'))


% define the different names
scale_name      = {'_noScale',''};
pcorr_name      = {'_uncorrected',''};

% set the parcellation
parcellations = {'Glasser2016','AAL','Brainnetome2016','Glasser2016_Buckner2011'};

% set the scaling to zero
scale = 0;


% initialize the results cells
Amatrix_allSubjects_AwA = [];
Cmatrix_allSubjects_AwA = [];


% close all figures
close all

% display progress
disp('Loading data...')


% get the data from aspirin and no aspirin group
for aspirin = [0 1]

    % set the path to the experiment
    foldername = fullfile(exp_foldername, aspirin_folder{aspirin+1});
    

    % load the results file
    temp = load(fullfile(foldername,'DCM_GroupAnalysis',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome'], ['PosteriorParameterEstimates_model' num2str(model) '.mat']));
    
    
    % create results array
    if ( isempty(Amatrix_allSubjects_AwA) )
        Amatrix_allSubjects_AwA = cell(size(temp.results.A_Matrix_AllSubjects,1),size(temp.results.A_Matrix_AllSubjects,2),2);
        Cmatrix_allSubjects_AwA = cell(size(temp.results.C_Matrix_AllSubjects,1),size(temp.results.C_Matrix_AllSubjects,2),2);
    end

    % asign the data
    for handedness = 1:2
        for int = 1:size(temp.results.A_Matrix_AllSubjects,1)
            for int2 = 1:size(temp.results.A_Matrix_AllSubjects,2)
                Amatrix_allSubjects_AwA{int,int2,aspirin+1}(:,handedness)	= temp.results.A_Matrix_AllSubjects{int,int2,handedness};
            end
        end

        for int = 1:size(temp.results.C_Matrix_AllSubjects,1)
            for int2 = 1:size(temp.results.C_Matrix_AllSubjects,2)
                Cmatrix_allSubjects_AwA{int,int2,aspirin+1}(:,handedness)	= temp.results.C_Matrix_AllSubjects{int,int2,handedness};
            end
        end
    end
end


% specify a indicator vector
vector_woAspirin = [1:7 10:size(Amatrix_allSubjects_AwA{1,1,1},1)];
vector_Aspirin   = [1:4 7:size(Amatrix_allSubjects_AwA{1,1,2},1)];


% initialize the matrices
pVal_Amatrix_allSubjects_AwA = NaN(size(Amatrix_allSubjects_AwA,1),size(Amatrix_allSubjects_AwA,2));
mean_Amatrix_allSubjects_AwA = NaN(size(Amatrix_allSubjects_AwA,1),size(Amatrix_allSubjects_AwA,2));
pVal_Cmatrix_allSubjects_AwA = NaN(size(Cmatrix_allSubjects_AwA,1),size(Cmatrix_allSubjects_AwA,2));
mean_Cmatrix_allSubjects_AwA = NaN(size(Cmatrix_allSubjects_AwA,1),size(Cmatrix_allSubjects_AwA,2));


% dsiplay progress
disp('Computing aspirin vs no-aspirin difference...')

% reverse string
reverseStr = '';

% where to store the pvalues
store_filename = fullfile(exp_foldername,'all_Aspirin','DCM_GroupAnalysis',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome'],['Model' num2str(model) '_Difference_AwA' pcorr_name{p_corr+1} '.mat']);

% ANOVA for the endogenous connectivity parameters
for int = 1:size(Amatrix_allSubjects_AwA,1)

    % display progress
    msg = sprintf('Processed %d/%d', int, size(Amatrix_allSubjects_AwA,1));
    fprintf([reverseStr, msg]);
    reverseStr = repmat(sprintf('\b'), 1, length(msg));
    
    for int2 = 1:size(Amatrix_allSubjects_AwA,2)
        
        % generate the data array for the ANOVA
        data_array = [Amatrix_allSubjects_AwA{int,int2,1}(vector_woAspirin,:); Amatrix_allSubjects_AwA{int,int2,2}(vector_Aspirin,:)];

        % generate the between-factor indicator
        between_factors = [ones(length(vector_woAspirin),1); 2*ones(length(vector_Aspirin),1)];

        % compute a mixed-effects ANOVA with (1 within- and 1 between-subject factor)
        if ( any(data_array~=0) )
            tbl = simple_mixed_anova(data_array, between_factors, {'Hand'}, {'Aspirin'});
            p   = tbl{2,5};
        else
            p   = NaN;
        end

        % compute the mean
        mean_Amatrix_allSubjects_AwA(int,int2) = mean(mean(Amatrix_allSubjects_AwA{int,int2,2}(vector_Aspirin,:),1))-mean(mean(Amatrix_allSubjects_AwA{int,int2,1}(vector_woAspirin,:),1));

        % asign the p-value
        if ( any(Amatrix_allSubjects_AwA{int,int2,1} ~= 0) )
            pVal_Amatrix_allSubjects_AwA(int,int2) = p;
        end
    end
end

% two-sample t-test for the driving input parameters
for int = 1:size(Cmatrix_allSubjects_AwA,1)
    for int2 = 1:size(Cmatrix_allSubjects_AwA,2)

        % generate the data array for the ANOVA
        data_array = [Cmatrix_allSubjects_AwA{int,int2,1}(vector_woAspirin,:); Cmatrix_allSubjects_AwA{int,int2,2}(vector_Aspirin,:)];

        % generate the between-factor indicator
        between_factors = [ones(length(vector_woAspirin),1); 2*ones(length(vector_Aspirin),1)];

        % compute a mixed-effects ANOVA with (1 within- and 1 between-subject factor)
        if ( any(data_array~=0) )
            tbl = simple_mixed_anova(data_array, between_factors, {'Hand'}, {'Aspirin'});
            p   = tbl{2,5};
        else
            p   = NaN;
        end

        % compute the mean
        mean_Cmatrix_allSubjects_AwA(int,int2) = mean(mean(Cmatrix_allSubjects_AwA{int,int2,2}(vector_Aspirin,:),1))-mean(mean(Cmatrix_allSubjects_AwA{int,int2,1}(vector_woAspirin,:),1));
        
        % asign the p-value
        if ( any(Cmatrix_allSubjects_AwA{int,int2,1} ~= 0) )
            pVal_Cmatrix_allSubjects_AwA(int,int2) = p;
        end
    end
end


% set the threshold
if ( p_corr )
    p_all             = [pVal_Amatrix_allSubjects_AwA(:); pVal_Cmatrix_allSubjects_AwA(:)];
    p_all             = p_all(isfinite(p_all));
    [~, crit_p, ~, ~] = fdr_bh(p_all,0.05,'dep','no');
    threshold         = crit_p;
    disp('')
    disp(['Estimated p-Value (FDR-corrected): ' num2str(threshold)])
else
    threshold = 0.001;
    disp('')
    disp(['Estimated p-Value (uncorrected): ' num2str(threshold)])
end


% get the "significant" differences
binary_pVal_Amatrix = pVal_Amatrix_allSubjects_AwA <= threshold;
binary_pVal_Cmatrix = pVal_Cmatrix_allSubjects_AwA <= threshold;


% specify the folder where output logs should be stored
logs_folder = fullfile(exp_foldername,'Logs',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome']);

% create the results folder
if ( ~exist(logs_folder,'dir') )
    mkdir(logs_folder)
end

% open a file
fileID = fopen(fullfile(logs_folder, ['Model' num2str(model) '_Difference_AwA_List' pcorr_name{p_corr+1} '.txt']),'w');

% write the connectivity differences
fprintf(fileID,'Effect of Aspirin: Connectivity \n');
if ( any(binary_pVal_Amatrix(:)==1) )
    for int = 1:size(binary_pVal_Amatrix,2)
        for int2 = 1:size(binary_pVal_Amatrix,1)
            if ( binary_pVal_Amatrix(int2,int) == 1 )
                fprintf(fileID,[temp.results.AllRegions{int} ' --> ' temp.results.AllRegions{int2} ': \t%f\t%f\n'], mean_Amatrix_allSubjects_LR(int2,int), pVal_Amatrix_allSubjects_LR(int2,int));
            end
        end
    end
else
    fprintf(fileID,'No significant differences between Aspirin and wo_Aspirin');
end

% write the driving input differences
fprintf(fileID,'\nEffect of Handedness: Driving input \n');
if ( any(binary_pVal_Cmatrix(:)==1) )
    for int2 = 1:size(binary_pVal_Cmatrix,1)
        if ( binary_pVal_Cmatrix(int2,1) == 1 )
            fprintf(fileID,['driving input --> ' temp.results.AllRegions{int2} ': \t\t%f\t%f\n'], mean_Cmatrix_allSubjects_LR(int2,1), pVal_Cmatrix_allSubjects_LR(int2,1));
        end
    end
else
    fprintf(fileID,'No significant differences between Aspirin and wo_Aspirin');
end

% close the file
fclose(fileID);


% create the connectogram
mean_Amatrix_allSubjects_AwA_store   = mean_Amatrix_allSubjects_AwA .* (pVal_Amatrix_allSubjects_AwA <= threshold);


% store the connectogram
if ( ~isempty(mean_Amatrix_allSubjects_AwA_store) )
    
    % create folder
    if ( ~exist(fullfile(exp_foldername,aspirin_folder{aspirin+1},'DCM_GroupAnalysis',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome']),'dir') )
        mkdir(fullfile(exp_foldername,aspirin_folder{aspirin+1},'DCM_GroupAnalysis',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome']))
    end
    
    % store the connectogram
    save(store_filename,'pVal_Amatrix_allSubjects_AwA','pVal_Cmatrix_allSubjects_AwA','mean_Amatrix_allSubjects_AwA_store')
end

end