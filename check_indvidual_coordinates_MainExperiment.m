function check_indvidual_coordinates_MainExperiment(parcellation_type)
% Get the center coordinate of the regions of interest (ROIs) for each 
% subject individually. The function checks which ROIs were missing in at
% least one of the subjects and excludes those ROIs from further
% connectivity analysis. The funciton also plots a 3D image illustrating 
% the location of the individual ROIs to visually inspect the coordinates 
% for outliers.
% 
% Input:
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainnetome 2016, (4) Glasser 2016 & Buckner 2011
% 
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% add the colormap
addpath(fullfile(FilenameInfo.MATLAB_path,'bipolar_colormap'))


% close all figures
close all

% set the different parcellation options
parcellations = {'Glasser2016','AAL','Brainnetome2016','Glasser2016_Buckner2011'};
handedness_folder = {'left','right'};


% create figure folder
if ( ~exist(fullfile(FilenameInfo.DataPath,'Figures','IBS',parcellations{parcellation_type}),'dir') )
    mkdir(fullfile(FilenameInfo.DataPath,'Figures','IBS',parcellations{parcellation_type}))
end


% iterate over hand movement conditions
for handedness = 1:2

    % load the coordinates file
    temp                  = load(fullfile(FilenameInfo.DataPath,'VoI_coordinates','IBS',parcellations{parcellation_type},'VoI_CenterCoordinates_AllSubjects.mat'));
    center_allregions     = temp.center_allregions;
    center_allregions_new = cell(size(center_allregions,2),1);


    % get center coordinates
    for aspirin = 1:size(center_allregions,3)
        
        % restrict set of participants
        if ( aspirin == 1 )
            vector = [1:7 10:14];
            Nr_wo_aspirin = length(vector);
        elseif ( aspirin == 2 )
            vector = [1:4 7:15];
        end
        
        % get coordinates for all participants
        for subject = 1:length(vector)
            
            % store the coordinates in new structure
            for region = 1:size(center_allregions,2)
                if ( ~isempty(center_allregions{vector(subject),region,aspirin,handedness}) & (isfinite(center_allregions{vector(subject),region,aspirin,handedness})) )
                    center_allregions_new{region}(subject+Nr_wo_aspirin*(aspirin-1),:) = center_allregions{vector(subject),region,aspirin,handedness};
                else
                    center_allregions_new{region}(subject+Nr_wo_aspirin*(aspirin-1),:) = [NaN NaN NaN];
                end
            end
        end
    end
    
    
    % specify array to store all missing regions
    missing_ROIs = NaN(size(center_allregions_new{1},1),length(center_allregions_new));
    
    
    % find all missing regions
    for int = 1:size(missing_ROIs,1)
        for int2 = 1:size(missing_ROIs,2)
            if ( isempty(center_allregions_new{int2}(int,:)) )
                missing_ROIs(int,int2) = 2;
            else
                if ( ~any(isfinite(center_allregions_new{int2}(int,:))) )
                    missing_ROIs(int,int2) = 1;
                else
                    missing_ROIs(int,int2) = 0;
                end
            end
        end
    end

    % exclude subjects where no time series were extracted
    indices = find(sum(missing_ROIs,2)~=size(missing_ROIs,2));

    % compute the number of regions present in all subjects
    NR_ROIs = sum(sum(missing_ROIs(indices,:))==0);
    ind_ROIs = find(sum(missing_ROIs(indices,:))~=0);
    
    
    % check if ps-file exists
    if ( exist(fullfile(FilenameInfo.DataPath,'Figures','IBS',parcellations{parcellation_type},'VOI_coordinates.ps'),'file') )
        delete(fullfile(FilenameInfo.DataPath,'Figures','IBS',parcellations{parcellation_type},'VOI_coordinates.ps'))
    end
    
    
    % plot the missing regions
    figure('units','normalized','outerposition',[0 0 1 1]);
    imagesc(missing_ROIs)
    hold on
    plot([1 size(missing_ROIs,2)],[Nr_wo_aspirin+0.5 Nr_wo_aspirin+0.5],'k-','LineWidth',3)
    axis square
    colormap('bipolar')
    caxis([-1 1])
    ylabel('subjects','FontSize',14)
    xlabel('regions','FontSize',14)
    title([handedness_folder{handedness} ' - Overview of ROIs'],'FontSize',14)
    print(gcf, '-dpsc', fullfile(FilenameInfo.DataPath,'Figures','IBS',parcellations{parcellation_type},'VOI_coordinates'), '-append')
    
    
    % get names of ROIs
    mask_info = create_mask_information_MainExperiment(parcellation_type);


    % display the number of subjects in the restricted sample
    fprintf('\nFound: %d subjects with %d regions\n',length(indices),NR_ROIs)
    fprintf(['Missing regions: ' repmat('%s | ',1,length(ind_ROIs)) '\n'], mask_info{ind_ROIs})


    % get and store the missing regions
    allMissing_regions     = mask_info(ind_ROIs);
    allMissing_regions_ind = [];
    for int = 1:length(ind_ROIs)
        if ( isempty(allMissing_regions_ind) )
            allMissing_regions_ind(1) = ind_ROIs(int);
        else
            allMissing_regions_ind(end+1) = ind_ROIs(int);
        end

        if ( mod(ind_ROIs(int),2) == 1)
            allMissing_regions_ind(end+1) = ind_ROIs(int)+1;
        else
            allMissing_regions_ind(end+1) = ind_ROIs(int)-1;
        end
    end

    allMissing_regions_ind = unique(allMissing_regions_ind);
    
    if ( handedness == 1 )
        allMissing_regions_ind_left = allMissing_regions_ind;
    elseif ( handedness == 2 )
        allMissing_regions_ind_right = allMissing_regions_ind;
    end
    
    
    % set the color code
    figure('units','normalized','outerposition',[0 0 1 1]);
    rng(0);
    NR_regions  = size(center_allregions,2);
    a = colormap(jet(NR_regions/2));
    randomized_order = randperm(size(a,1));
    a = a(randomized_order,:);
    color_style = NaN(size(a,1)*2,size(a,2));
    color_style(1:2:end,:) = a;
    color_style(2:2:end,:) = a;


    % plot the location of the regions in a 3D plot
    title([handedness_folder{handedness} ' - Individual Coordinates of ROIs'])
    for regions = 1:length(center_allregions_new)
        plot3(center_allregions_new{regions}(:,1),center_allregions_new{regions}(:,2),center_allregions_new{regions}(:,3),'.','Color',color_style(regions,:))
        hold on
    end
    view(-52,12)
    xlabel('x [mm]')
    ylabel('y [mm]')
    zlabel('z [mm]')
    xlim([-80 80])
    ylim([-100 80])
    zlim([-60 100])
    axis square
    print(gcf, '-dpsc', fullfile(FilenameInfo.DataPath,'Figures','IBS',parcellations{parcellation_type},'VOI_coordinates'), '-append')


    % plot the location of the regions in the horizontal view
    figure('units','normalized','outerposition',[0 0 1 1])
    for regions = 1:length(center_allregions_new)
        plot(center_allregions_new{regions}(:,1),center_allregions_new{regions}(:,2),'.','Color',color_style(regions,:))
        hold on
    end
    xlabel('x [mm]','FontSize',12)
    ylabel('y [mm]','FontSize',12)
    xlim([-80 80])
    ylim([-100 80])
    axis square
    title([handedness_folder{handedness} ' - Individual Coordinates of ROIs [horizontal]'])
    print(gcf, '-dpsc', fullfile(FilenameInfo.DataPath,'Figures','IBS',parcellations{parcellation_type},'VOI_coordinates'), '-append')


    % plot the location of the regions in the sagital view
    figure('units','normalized','outerposition',[0 0 1 1]);
    for regions = 1:length(center_allregions_new)
        plot(center_allregions_new{regions}(:,2),center_allregions_new{regions}(:,3),'.','Color',color_style(regions,:))
        hold on
    end
    xlabel('y [mm]','FontSize',12)
    ylabel('z [mm]','FontSize',12)
    xlim([-100 80])
    ylim([-60 100])
    axis square
    title([handedness_folder{handedness} ' - Individual Coordinates of ROIs [sagittal]'])
    print(gcf, '-dpsc', fullfile(FilenameInfo.DataPath,'Figures','IBS',parcellations{parcellation_type},'VOI_coordinates'), '-append')



    % plot the location of the regions in the horizontal view
    figure('units','normalized','outerposition',[0 0 1 1]);
    for regions = 1:2:length(center_allregions_new)
        plot(mean([center_allregions_new{regions}(1,2) center_allregions_new{regions+1}(1,2)]),mean([center_allregions_new{regions}(1,3) center_allregions_new{regions+1}(1,3)]),'.','Color',color_style(regions,:))
        hold on
        if ( regions <= 360 )
            text(mean([center_allregions_new{regions}(1,2) center_allregions_new{regions+1}(1,2)]),mean([center_allregions_new{regions}(1,3) center_allregions_new{regions+1}(1,3)]),['R' num2str(regions) ': ' strrep(mask_info{regions,1}(1:end-5),'_','\_')],'FontSize',6)
        else
            text(mean([center_allregions_new{regions}(1,2) center_allregions_new{regions+1}(1,2)]),mean([center_allregions_new{regions}(1,3) center_allregions_new{regions+1}(1,3)]),['R' num2str(regions) ': Cereb' strrep(mask_info{regions,1}(end-5),'_','\_')],'FontSize',6)
        end
    end
    xlabel('y [mm]','FontSize',12)
    ylabel('z [mm]','FontSize',12)
    xlim([-100 80])
    ylim([-60 100])
    axis square
    title([handedness_folder{handedness} ' - Individual Coordinates of ROIs [sagittal]'])
    print(gcf, '-dpsc', fullfile(FilenameInfo.DataPath,'Figures','IBS',parcellations{parcellation_type},'VOI_coordinates'), '-append')
    
end


% check whether missing regions are identical for both hand movements
if ( ~isequal(allMissing_regions_ind_left,allMissing_regions_ind_right) )
    disp('CAUTION: Missing regions not matching! Double-check...')
    return
end

% if missing regions are identical - store them
if ( ~isempty(allMissing_regions) && ~isempty(allMissing_regions_ind) )
    save(fullfile(FilenameInfo.DataPath,'VoI_coordinates','IBS',parcellations{parcellation_type},'MissingROIs.mat'),'allMissing_regions','allMissing_regions_ind')
end


% get all the regions
region_ind = 1:size(missing_ROIs,2);

% get the included regions
region_ind_cut = setdiff(region_ind,allMissing_regions_ind);


% convert into the BrainNet format
fileBrainNet = fullfile(FilenameInfo.DataPath,'VoI_coordinates','IBS',parcellations{parcellation_type},'AllCoordinates.node');
if ( exist(fileBrainNet,'file') )
    delete(fileBrainNet)
end

% open an empty file
fileID = fopen(fileBrainNet,'w');

% get randomized color
rng(2406);
color_BrainNet = randperm(length(region_ind_cut)/2);

% write the mean coordinates
for region = 1:length(region_ind_cut)
    for subject = 1:size(center_allregions_new{region_ind_cut(region)},1)
        
        % get the coordiantes for each region in every subject
        x_coord = center_allregions_new{region_ind_cut(region)}(subject,1);
        y_coord = center_allregions_new{region_ind_cut(region)}(subject,2);
        z_coord = center_allregions_new{region_ind_cut(region)}(subject,3);
        
        % write the coordinate and set the color
        fprintf(fileID,[num2str(x_coord) '\t' num2str(y_coord) '\t' num2str(z_coord) '\t' num2str(color_BrainNet(ceil(region/2))) '\t' '1\n']);
    
    end
end

% close the node file again
fclose(fileID);

end
