function compute_signal_rDCM_MainExperiment(subject,model,aspirin,handedness,restrictInputs,parcellation_type,prior,onCluster)
% Helper script for the analysis of the whole-brain effective connectivity 
% for the Hand Movement dataset of the Stroke study using regression DCM.
% 
% This function is a small helper script that takes the inversted DCMs from
% rDCM and redos the computation of the predicted signal.
% 
% Input:
%   subject             -- subject to analyze
%   model               -- model to analyze
%   aspirin             -- (0) no aspirin and (1) aspirin
%   handedness          -- (1) left hand, (2) right hand
%   restrictInputs      -- fix inputs to the "correct" regions
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL
%   prior               -- (0) DCM10 priors,(1) wide priors
%   cores               -- how many cores for estimation
%   onCluster           -- operation system: (1) Euler, (0) local machine
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2018
% Copyright 2018 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% define the experiment folders
aspirin_folder    = {'wo_Aspirin/','Aspirin/'};
handedness_folder = {'left','right'};


% on Euler or not? add the relevant paths and set the foldername
if ( onCluster == 1 )
    addpath(genpath('/cluster/home/stefanf/matlab/rDCM/matlab/trunk'))
    addpath(genpath('/cluster/home/stefanf/matlab/rDCM/matlab/GIT_files'))
    addpath(genpath('/cluster/home/stefanf/matlab/rDCM/matlab/analysis'))
    addpath(genpath('/cluster/home/stefanf/matlab/rDCM/matlab/rdcm'))
    addpath '/cluster/home/stefanf/matlab/spm8v/';
    pre_foldername  = '/cluster/scratch/stefanf/rDCM_Stroke/';
else
    addpath('/Users/stefanf/Documents/SPM/spm8v/')
    pre_foldername = '/Users/stefanf/Documents/ETH Zurich/Projects/rDCM_Stroke/';
end


% define the different names
foldername      = [pre_foldername aspirin_folder{aspirin+1}];
scale_name      = {'_noScale',''};
input_name      = {'allInput','restrictInputs'};
prior_name      = {'','wide_prior'};

% define the filename
filename = ['DCM_hand_model' num2str(model)];


% define the standard option settings
options.scale      = 0;
options.estimateVL = 0;

% asign the scale
scale = options.scale;


% get the subject list
Subject_List = [dir(fullfile(foldername,'1*')); dir(fullfile(foldername,'2*')); dir(fullfile(foldername,'3*'))];

% subject name
Subject = Subject_List(subject).name;

% display subject name
disp(['Subject: ' Subject])

% clear any DCM structure
clear DCM

% set the parcellation
parcellations     = {'Glasser2016','AAL'};

% load the SPM
load(fullfile(foldername, Subject, 'FirstLevel', 'IBS', ['tapping_' handedness_folder{handedness}], 'SPM.mat'))

% asign the data of the respective subject to the DCM
VOI_foldername = fullfile(foldername, Subject, 'FirstLevel', 'IBS', ['tapping_' handedness_folder{handedness}], 'timeseries_VOI_adjusted', parcellations{parcellation_type});

% remove ROIs that are not present in all subjects
if ( parcellation_type == 1 )
    delete(fullfile(VOI_foldername,'VOI_10pp_*.mat'))
    delete(fullfile(VOI_foldername,'VOI_10r_*.mat'))
    delete(fullfile(VOI_foldername,'VOI_10v_*.mat'))
    delete(fullfile(VOI_foldername,'VOI_13l_*.mat'))
    delete(fullfile(VOI_foldername,'VOI_25_*.mat'))
    delete(fullfile(VOI_foldername,'VOI_FFC_*.mat'))
    delete(fullfile(VOI_foldername,'VOI_LO2_*.mat'))
    delete(fullfile(VOI_foldername,'VOI_OFC_*.mat'))
    delete(fullfile(VOI_foldername,'VOI_PIT_*.mat'))
    delete(fullfile(VOI_foldername,'VOI_PeEc_*.mat'))
    delete(fullfile(VOI_foldername,'VOI_STSda_*.mat'))
    delete(fullfile(VOI_foldername,'VOI_STSva_*.mat'))
    delete(fullfile(VOI_foldername,'VOI_TE1a_*.mat'))
    delete(fullfile(VOI_foldername,'VOI_TE1m_*.mat'))
    delete(fullfile(VOI_foldername,'VOI_TE2a_*.mat'))
    delete(fullfile(VOI_foldername,'VOI_TE2p_*.mat'))
    delete(fullfile(VOI_foldername,'VOI_TF_*.mat'))
    delete(fullfile(VOI_foldername,'VOI_TGv_*.mat'))
    delete(fullfile(VOI_foldername,'VOI_V8_*.mat'))
    delete(fullfile(VOI_foldername,'VOI_VMV3_*.mat'))
    delete(fullfile(VOI_foldername,'VOI_a10p_*.mat'))
    delete(fullfile(VOI_foldername,'VOI_p10p_*.mat'))
    delete(fullfile(VOI_foldername,'VOI_s32_*.mat'))
end

% get the VOI file in the folder
VOI_files = dir(fullfile(VOI_foldername, 'VOI*.mat'));

% display progress
disp('Loading data...')
disp(['Found number of regions: ' num2str(length(VOI_files))])

% load the VOI time series for all regions of interes                                                                                                                            
for number_of_regions = 1:length(VOI_files)

    % load the files
    load(fullfile(VOI_foldername,VOI_files(number_of_regions).name),'xY');
    DCM.xY(number_of_regions) = xY;

end

% number of regions
DCM.n = length(DCM.xY);

% number of time points
DCM.v = length(DCM.xY(1).u);

% specify the TR
DCM.Y.dt  = SPM.xY.RT;

% specify the Y component of the DCM file
DCM.Y.X0 = DCM.xY(1).X0;

% asign the data to the Y structure
for i = 1:DCM.n
    DCM.Y.y(:,i)  = DCM.xY(i).u;
    DCM.Y.name{i} = DCM.xY(i).name;
end

% define the covariance matrix
DCM.Y.Q = spm_Ce(ones(1,DCM.n)*DCM.v);

% Experimental inputs
DCM.U.dt   = SPM.Sess.U(1).dt;
DCM.U.name = [SPM.Sess.U.name];
DCM.U.u    = SPM.Sess.U(1).u(33:end,1);

% DCM parameters
DCM.delays = repmat(SPM.xY.RT/2,DCM.n,1);
DCM.TE     = 0.0250;

% DCM options
DCM.options.nonlinear  = 0;
DCM.options.two_state  = 0;
DCM.options.stochastic = 0;
DCM.options.nograph    = 0;

% Connectivity matrices for the respective models
if ( model == 1 )
    DCM.a = ones(DCM.n,DCM.n);
    DCM.b = zeros(DCM.n,DCM.n,size(DCM.U.u,2));
    DCM.c = ones(DCM.n,size(DCM.U.u,2));
    DCM.d = zeros(DCM.n,DCM.n,0);
end


% detrend and scale the data
if ( options.scale )
    
    % detrend data
    DCM.Y.y = spm_detrend(DCM.Y.y);

    % scale data
    scale   = max(max((DCM.Y.y))) - min(min((DCM.Y.y)));
    scale   = 4/max(scale,4);
    DCM.Y.y = DCM.Y.y*scale;

end


% set the padding
options.padding = 0;

% use spm to estimate the computed signal
options.compute_signal_spm = 1;

% create the options file
options = rdcm_set_options(DCM, options, 'r');


% set the p0 values
p0_all 	= 0.05:0.05:0.95;


if ( restrictInputs == 0 )
    options.u_shift = 0;
end

% create the regressors
[~, ~, DCM, args] = rdcm_create_regressors(DCM, options);

% set the input option
args.restrictInputs = restrictInputs;

% define the inputs
if ( restrictInputs )
    args.input = DCM.c;
end

% default prior settings
if ( isempty(prior) )
    prior = 0;
end

% set the prior type of the endogenous matrix
DCM.options.wide_priors = prior;


% display the progress
disp(['Loading rDCM: subject ' num2str(subject) ' - model ' num2str(model)])
disp(' ')

% load the DCM
temp   = load(fullfile(foldername, Subject, 'FirstLevel_DCM', ['tapping_' handedness_folder{handedness}], parcellations{parcellation_type}, ['regressionDCM' scale_name{scale+1}], input_name{restrictInputs+1}, prior_name{prior+1}, [filename '_rDCM.mat']));
output = temp.output;

% free energy values 
F_all = NaN(1,length(output));


% evaluate the results
for p0_counter = 1:length(output)
    
    % now compute of the signal
    disp(['Compute signal: p0 = ' num2str(p0_all(p0_counter))])
    output{p0_counter} = rdcm_compute_signals(DCM, output{p0_counter}, options);
    
    % get the negative free energy
    F_all(p0_counter)   = output{p0_counter}.logF;
    
end


% get the setting with max free energy
[~, F_max_ind] = max(F_all);

% save the best setting
output_best{1} = output{F_max_ind};

% save the estimated results
if ( ~isempty(output_best) )
    save(fullfile(foldername, Subject, 'FirstLevel_DCM', ['tapping_' handedness_folder{handedness}], parcellations{parcellation_type}, ['regressionDCM' scale_name{scale+1}], input_name{restrictInputs+1}, prior_name{prior+1}, [filename '_rDCM.mat']),'output','output_best')
end

end
