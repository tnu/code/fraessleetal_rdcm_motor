function extract_VOI_timeseries_MainExperiment(subject,handedness,aspirin,parcellation_type)
% Extracts the first principle eigenvariate from pre-specified regions of
% interest (ROI), which are given by masks.
%
% Input:
%   subject             -- subject to be analyzed
% 	handedness         	-- left- or right-hand movement dataset
%   aspirin             -- subjects (0) without or (1) with aspirin
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainnetome 2016, (4) Glasser 2016 & Buckner 2011
% 
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2018
% Copyright 2018 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define important folders
aspirin_folder    = {'wo_Aspirin/','Aspirin/'};
handedness_folder = {'left','right'};

% set the paths
addpath(genpath(FilenameInfo.SPM_path));

% specify folders
folder_experiment = fullfile(FilenameInfo.LongTermStorage_path,aspirin_folder{aspirin+1});
folder_results    = fullfile(FilenameInfo.DataPath,aspirin_folder{aspirin+1});
folder_batch      = FilenameInfo.Batch_path;


% get the subject list
Subject_List = dir(fullfile(folder_experiment,'1*'));

% subject name
Subject = Subject_List(subject).name;

% define the SPM folder name
SPM_foldername = fullfile(folder_results, Subject, 'FirstLevel', 'IBS', ['tapping_' handedness_folder{handedness}]);

% delete previous files
delete(fullfile(SPM_foldername, 'VOI*'))

% initialize spm
spm_jobman('initcfg')

% specify the effect of interest
EoI_nr = 2;


% display the progress
fprintf('Getting whole-brain parcellation information...\n')

% get the mask information
mask_info = create_mask_information_MainExperiment(parcellation_type);


% extract the time series for each region of interest
for number_of_regions = 1:size(mask_info,1)

    % load the batch for extracting the time series
    load(fullfile(folder_batch,'Batch_VoiExtraction.mat'))
    
    % define the path where the SPM.mat can be found
    matlabbatch{1}.spm.util.voi.spmmat = cellstr(fullfile(SPM_foldername,'SPM.mat'));

    % choose an adjustment of the time series (effects of interest)
    matlabbatch{1}.spm.util.voi.adjust = EoI_nr;

    % choose session number
    matlabbatch{1}.spm.util.voi.session = 1;

    % set the name of the VOI
    matlabbatch{1}.spm.util.voi.name = mask_info{number_of_regions,1};
    
    % specify which contrast to use
    matlabbatch{1}.spm.util.voi.roi{1}.spm.contrast = 1;
    
    % specify the threshold
    matlabbatch{1}.spm.util.voi.roi{1}.spm.threshdesc = 'none';
    matlabbatch{1}.spm.util.voi.roi{1}.spm.thresh     = 1;
	
    % define the filename of the mask
    matlabbatch{1}.spm.util.voi.roi{2}.mask.image = cellstr(mask_info{number_of_regions,2});
    
    % define the threshold for the mask
    matlabbatch{1}.spm.util.voi.roi{2}.mask.threshold = 0.5;
    
    % intersection of activation and sphere
    matlabbatch{1}.spm.util.voi.expression = 'i1 & i2';
    
    % specify input and output filenames
    Vi = [cellstr(fullfile(SPM_foldername,'con_0001.nii')); cellstr(mask_info{number_of_regions,2})];
    Vo = fullfile(SPM_foldername,'output.nii');
    
    % compute the intersection
    Vo = spm_imcalc(Vi, Vo, '(i1.*i2) ~= 0 & isfinite(i1.*i2)');
    
    % load the output file 
    intersection = spm_read_vols(Vo);
    
    % run the batch and extract the time series (in case there is data)
    if ( sum(intersection(:)) > 20 )
        spm_jobman('run',matlabbatch);
    else
        fprintf('\n WARNING: NO VOXELS FOUND IN ROI\n\n')
    end
    
    % pause briefly to show extracted time series
    pause(2)
    
end


% set the different parcellation options
parcellations = {'Glasser2016','AAL','Brainnetome2016','Glasser2016_Buckner2011'};

% time series folder
timeseries_folder = fullfile(SPM_foldername, 'timeseries_VOI_adjusted', parcellations{parcellation_type});

% check whether folder exists
if ( ~exist(timeseries_folder,'dir') )
    mkdir(timeseries_folder)
end

% move the extracted time series to the results folder
movefile(fullfile(SPM_foldername, 'VOI*.mat'),timeseries_folder)

% delete the remaining files
delete(fullfile(SPM_foldername, 'VOI*'))
