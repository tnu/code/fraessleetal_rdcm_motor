function mask_info = create_mask_information_MainExperiment(parcellation_type)
% Create a cell array that contains all the relevant information on the 
% mask that have been created for the whole-brain effective connectivity 
% analysis using regression dynamic causal modelling (srDCM). The masks are 
% based on the parcellation scheme that has been used.
%
% Input:
%   parcellation_type	-- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Buckner 2011, (4) Glasser 2016 & Buckner 2011
% 
% Output:
%   mask_info               -- relevant information of the masks
% 
% 
% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2018
% Copyright 2018 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% set the different parcellation options
parcellations = {'Glasser2016','AAL','Brainnetome2016','Buckner2011'};

% set the mask foldernmae
mask_foldername = fullfile(FilenameInfo.LongTermStorage_path,'parcellation');


% Glasser + Cerebellum
if ( parcellation_type == 4 )
 	parcellation_type_analyze = [1 4];
else
    parcellation_type_analyze = parcellation_type;
end

% get all the masks
for parcellation_type = 1:length(parcellation_type_analyze)
    
    temp = dir(fullfile(mask_foldername, parcellations{parcellation_type_analyze(parcellation_type)}, '/all_masks/*.img'));
    if ( isempty(temp) )
        temp = dir(fullfile(mask_foldername, parcellations{parcellation_type_analyze(parcellation_type)}, '/all_masks/*.nii'));

        if ( isempty(temp) )
            disp('No mask could be found...');
            exit
        end
    end
    mask_files = {temp(:).name};

    % asign the mask names and filename
    for number_of_regions = 1:length(mask_files)
        
        % provide the name and filename
        if ( parcellation_type == 1 && number_of_regions == 1 )
            mask_info{1,1} = mask_files{number_of_regions}(1:end-4);
            mask_info{1,2} = fullfile(mask_foldername, parcellations{parcellation_type_analyze(parcellation_type)}, 'all_masks', mask_files{number_of_regions});
        else
            mask_info{end+1,1}  = mask_files{number_of_regions}(1:end-4);
            mask_info{end,2}    = fullfile(mask_foldername, parcellations{parcellation_type_analyze(parcellation_type)}, 'all_masks', mask_files{number_of_regions});
        end
    end
end

% display how many regions where found
disp(['# of regions found: ' num2str(length(mask_info))]) 
