function compare_LR_srDCM_MainExperiment(model,aspirin,parcellation_type,restrictInputs,p_corr,onCluster)
% Compare parameter estimates from the whole-brain Dynamic Causal Models 
% (DCMs) for left- and right-hand movement for the hand movement dataset. 
% Parameter estimates have been computed using regression DCM (rDCM).
% 
% This function reads the summary file containing all the DCM parameter 
% estimates that have been estimated using regression DCM. The function 
% then evaluates the difference between left and right hand.
% 
% Input:
%   model               -- model to analyze
%   aspirin             -- (0) no aspirin and (1) aspirin
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainnetome 2016, (4) Glasser 2016 & Buckner 2011
%   restrictInputs      -- inputs: (0) all inputs, (1) restrict inputs
%   p_corr              -- (0) uncorrected, (1) FDR-correction
%   onCluster           -- operating system: (1) Euler, (0) local machine
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the experiment folders
aspirin_folder    = {'wo_Aspirin/','Aspirin/','all_Aspirin/'};

% add the relevant paths and set the foldername
exp_foldername = FilenameInfo.DataPath;
addpath(fullfile(FilenameInfo.MATLAB_path,'fdr_bh'))
addpath(fullfile(FilenameInfo.MATLAB_path,'bipolar_colormap'))
addpath(fullfile(FilenameInfo.MATLAB_path,'circularGraph'))


% define the different names
scale_name      = {'_noScale',''};
input_name      = {'allInput','restrictInputs'};
pcorr_name      = {'_uncorrected','','_top'};

% set the parcellation
parcellations = {'Glasser2016','AAL','Brainnetome2016','Glasser2016_Buckner2011'};

% scale fMRI data (default: 0)
scale = 0;


% close all figures
close all

% display progress
disp('Loading data...')



if ( ~isempty(aspirin) )
    
    % set the SPM path and the path to the experiment
    foldername = [exp_foldername aspirin_folder{aspirin+1}];

    % load the results file
    temp = load(fullfile(foldername,'DCM_GroupAnalysis',parcellations{parcellation_type},['sparse_regressionDCM' scale_name{scale+1}], input_name{restrictInputs+1}, ['PosteriorParameterEstimates_model' num2str(model) '.mat']));


    % asign the data
    Amatrix_allSubjects_left     = temp.results.A_Matrix_AllSubjects(:,:,1);
    Cmatrix_allSubjects_left     = temp.results.C_Matrix_AllSubjects(:,:,1);
    Amatrix_allSubjects_right    = temp.results.A_Matrix_AllSubjects(:,:,2);
    Cmatrix_allSubjects_right	 = temp.results.C_Matrix_AllSubjects(:,:,2);
    
else
    
    % iterate over both conditions
    for aspirin = [0 1]
        
        % set the SPM path and the path to the experiment
        foldername = [exp_foldername aspirin_folder{aspirin+1}];

        % load the results file
        temp = load(fullfile(foldername,'DCM_GroupAnalysis',parcellations{parcellation_type},['sparse_regressionDCM' scale_name{scale+1}], input_name{restrictInputs+1}, ['PosteriorParameterEstimates_model' num2str(model) '.mat']));


        % asign the data
        if ( aspirin == 0 )
            Amatrix_allSubjects_left     = temp.results.A_Matrix_AllSubjects(:,:,1);
            Cmatrix_allSubjects_left     = temp.results.C_Matrix_AllSubjects(:,:,1);
            Amatrix_allSubjects_right    = temp.results.A_Matrix_AllSubjects(:,:,2);
            Cmatrix_allSubjects_right	 = temp.results.C_Matrix_AllSubjects(:,:,2);
        else
            for int = 1:size(temp.results.A_Matrix_AllSubjects,1)
                for int2 = 1:size(temp.results.A_Matrix_AllSubjects,2)
                    Amatrix_allSubjects_left{int,int2}  = [Amatrix_allSubjects_left{int,int2}; temp.results.A_Matrix_AllSubjects{int,int2,1}];
                    Amatrix_allSubjects_right{int,int2} = [Amatrix_allSubjects_right{int,int2}; temp.results.A_Matrix_AllSubjects{int,int2,2}];
                end
                
                for int2 = 1:size(temp.results.C_Matrix_AllSubjects,2)
                    Cmatrix_allSubjects_left{int,int2}  = [Cmatrix_allSubjects_left{int,int2}; temp.results.C_Matrix_AllSubjects{int,int2,1}];
                    Cmatrix_allSubjects_right{int,int2} = [Cmatrix_allSubjects_right{int,int2}; temp.results.C_Matrix_AllSubjects{int,int2,2}];
                end
            end
        end
    end
    
    % set aspirin condition to all subjects
    aspirin = 2;
    
end


% specify a indicator vector
if ( aspirin == 0 )
    vector = [1:7 10:size(Amatrix_allSubjects_left{1,1},1)];
elseif ( aspirin == 1 )
    vector = [1:4 7:size(Amatrix_allSubjects_left{1,1},1)];
elseif ( aspirin == 2 )
    vector = [[1:7 10:14] [1:4 7:15]+14];
end


% dsiplay progress
disp(['# subjects found: ' num2str(length(vector))])
disp('Computing left-right difference...')

% results arrays
mean_Amatrix_allSubjects_LR = NaN(size(Amatrix_allSubjects_left));
mean_Cmatrix_allSubjects_LR = NaN(size(Cmatrix_allSubjects_left));
pVal_Amatrix_allSubjects_LR = NaN(size(Amatrix_allSubjects_left));
pVal_Cmatrix_allSubjects_LR = NaN(size(Cmatrix_allSubjects_left));
TVal_Amatrix_allSubjects_LR	= NaN(size(Amatrix_allSubjects_left));


% get summaries
for int = 1:size(Amatrix_allSubjects_left,1)
    for int2 = 1:size(Amatrix_allSubjects_left,2)

        % compute the mean endogenous connectivity
        mean_Amatrix_allSubjects_LR(int,int2) = nanmean(Amatrix_allSubjects_left{int,int2}(vector)-Amatrix_allSubjects_right{int,int2}(vector));
        
        % check for a significant difference
        [~,P,~,STATS] = ttest(Amatrix_allSubjects_left{int,int2}(vector)-Amatrix_allSubjects_right{int,int2}(vector),0);
        pVal_Amatrix_allSubjects_LR(int,int2) = P;
        
        % store the T statistics
        if ( isfinite(STATS.tstat) )
            TVal_Amatrix_allSubjects_LR(int,int2) = STATS.tstat;
        else
            TVal_Amatrix_allSubjects_LR(int,int2) = 0;
        end
    end
end

for int = 1:size(Cmatrix_allSubjects_left,1)
    for int2 = 1:size(Cmatrix_allSubjects_left,2)

        % compute the mean driving input connectivity
        mean_Cmatrix_allSubjects_LR(int,int2) = nanmean(Cmatrix_allSubjects_left{int,int2}(vector)-Cmatrix_allSubjects_right{int,int2}(vector));

        % check for a significant difference
        [~,P,~,~] = ttest(Cmatrix_allSubjects_left{int,int2}(vector)-Cmatrix_allSubjects_right{int,int2}(vector),0);
        pVal_Cmatrix_allSubjects_LR(int,int2) = P;
        
    end
end


% compute the top connections (most significant connections)
top_Amatrix_allSubjects_LR = compute_top_connections_MainExperiment(TVal_Amatrix_allSubjects_LR,[],500);


% set the threshold
if ( p_corr )
    p_all             = [pVal_Amatrix_allSubjects_LR(:); pVal_Cmatrix_allSubjects_LR(:)];
    p_all             = p_all(isfinite(p_all));
    [~, crit_p, ~, ~] = fdr_bh(p_all,0.05,'dep','no');
    threshold         = crit_p;
    disp(['Estimated p-Value (FDR-corrected): ' num2str(threshold)])
else
    threshold = 0.001;
    disp(['Estimated p-Value (uncorrected): ' num2str(threshold)])
end


% specify the folder where results should be stored
results_folder = fullfile(exp_foldername,'Figures','IBS',parcellations{parcellation_type},['sparse_regressionDCM' scale_name{scale+1}],input_name{restrictInputs+1},'EffectOfHand',aspirin_folder{aspirin+1});

% create the directory
if ( ~exist(results_folder,'dir') ) 
    mkdir(results_folder)
else
    if ( exist(fullfile(results_folder,['Model' num2str(model) '_Difference_LR.ps']),'file') )
        delete(fullfile(results_folder,['Model' num2str(model) '_Difference_LR.ps']))
    end
end



% open the figures
figure('units','normalized','outerposition',[0 0 1 1])
figure('units','normalized','outerposition',[0 0 1 1])
figure('units','normalized','outerposition',[0 0 1 1])
figure('units','normalized','outerposition',[0 0 1 1])
figure('units','normalized','outerposition',[0 0 1 1])


% display differences in connectivity between left and right hand
figure(1)
imagesc(mean_Amatrix_allSubjects_LR-diag(diag(mean_Amatrix_allSubjects_LR)))
colormap('bipolar')
caxis([-0.005 0.005])
axis square
colorbar
title('mean endogenous connectivity (A matrix) - effect of hand','FontSize',12)
xlabel('region (from)','FontSize',11)
ylabel('region (to)','FontSize',11)
set(gca,'xtick',[1 size(mean_Amatrix_allSubjects_LR,1)/2 size(mean_Amatrix_allSubjects_LR,1)])
set(gca,'ytick',[1 size(mean_Amatrix_allSubjects_LR,1)/2 size(mean_Amatrix_allSubjects_LR,1)])
print(gcf, '-dpsc', fullfile(results_folder,['Model' num2str(model) '_Difference_LR']), '-append')

% display significant differences in connectivity between left and right hand
figure(2)
imagesc(pVal_Amatrix_allSubjects_LR <= threshold)
colormap('gray')
caxis([0 1])
axis square
colorbar
title('mean endogenous connectivity (A matrix) - (sig) effect of hand','FontSize',12)
xlabel('region (from)','FontSize',11)
ylabel('region (to)','FontSize',11)
set(gca,'xtick',[1 size(mean_Amatrix_allSubjects_LR,1)/2 size(mean_Amatrix_allSubjects_LR,1)])
set(gca,'ytick',[1 size(mean_Amatrix_allSubjects_LR,1)/2 size(mean_Amatrix_allSubjects_LR,1)])
print(gcf, '-dpsc', fullfile(results_folder,['Model' num2str(model) '_Difference_LR']), '-append')


% get the "significant" differences
if ( p_corr )
    binary_pVal_Amatrix = pVal_Amatrix_allSubjects_LR <= threshold;
    binary_pVal_Cmatrix = pVal_Cmatrix_allSubjects_LR <= threshold;
else
    binary_pVal_Amatrix = pVal_Amatrix_allSubjects_LR < threshold;
    binary_pVal_Cmatrix = pVal_Cmatrix_allSubjects_LR < threshold;
end


% specify the folder where output logs should be stored
logs_folder = fullfile(exp_foldername,'Logs',parcellations{parcellation_type},['sparse_regressionDCM' scale_name{scale+1}], input_name{restrictInputs+1});

% create the results folder
if ( ~exist(logs_folder,'dir') )
    mkdir(logs_folder)
end


% open a file
fileID = fopen(fullfile(logs_folder, ['Model' num2str(model) '_Difference_LR_List' pcorr_name{p_corr+1} '.txt']),'w');

% write the connectivity differences
fprintf(fileID,'Effect of Handedness: Connectivity \n');
for int = 1:size(binary_pVal_Amatrix,2)
    for int2 = 1:size(binary_pVal_Amatrix,1)
        if ( binary_pVal_Amatrix(int2,int) == 1 )
            fprintf(fileID,[temp.results.AllRegions{int} ' --> ' temp.results.AllRegions{int2} ': \t%f\t%f\n'], mean_Amatrix_allSubjects_LR(int2,int), pVal_Amatrix_allSubjects_LR(int2,int));
        end
    end
end

% write the driving input differences
fprintf(fileID,'\nEffect of Handedness: Driving input \n');
for int2 = 1:size(binary_pVal_Cmatrix,1)
    if ( binary_pVal_Cmatrix(int2,1) == 1 )
        fprintf(fileID,['driving input --> ' temp.results.AllRegions{int2} ': \t\t%f\t%f\n'], mean_Cmatrix_allSubjects_LR(int2,1), pVal_Cmatrix_allSubjects_LR(int2,1));
    end
end

% close the file
fclose(fileID);


% display the mean driving input connectivity
figure(3)
imagesc(mean_Cmatrix_allSubjects_LR);
colormap('bipolar')
caxis([-1*floor(max(max(abs(mean_Cmatrix_allSubjects_LR)))*10)/10 floor(max(max(abs(mean_Cmatrix_allSubjects_LR)))*10)/10])
axis square
colorbar
set(gca,'xtick',1)
set(gca,'xticklabel','')
title('mean driving input (C matrix) - effect of hand','FontSize',12)
xlabel('driving input','FontSize',11)
ylabel('region (to)','FontSize',11)
set(gca,'ytick',[1 size(mean_Cmatrix_allSubjects_LR,1)/2 size(mean_Cmatrix_allSubjects_LR,1)])

% get the top most driving inputs
NR_regions = 10;
[~,I] = sort(abs(mean_Cmatrix_allSubjects_LR),'descend');
I_sort = sort(I(1:NR_regions));
for int = 1:NR_regions
    text(0.6+(mod(int,4)*0.2),I_sort(int),strrep(temp.results.AllRegions{I_sort(int)}(6:end),'_','\_'),'FontSize',12,'FontWeight','bold')
end
print(gcf, '-dpsc', fullfile(results_folder,['Model' num2str(model) '_Difference_LR']), '-append')

figure(4)
imagesc(pVal_Cmatrix_allSubjects_LR <= threshold)
colormap('gray')
caxis([0 1])
axis square
colorbar
set(gca,'xtick',1)
set(gca,'xticklabel','')
title('mean driving input (C matrix) - (sig.) effect of hand','FontSize',12)
xlabel('driving input','FontSize',11)
ylabel('region (to)','FontSize',11)
set(gca,'ytick',[1 size(mean_Cmatrix_allSubjects_LR,1)/2 size(mean_Cmatrix_allSubjects_LR,1)])
print(gcf, '-dpsc', fullfile(results_folder,['Model' num2str(model) '_Difference_LR']), '-append')


% open figure
figure(5);

% threshold the difference matrix
if ( p_corr ~= 0 )
    
    % get the thresholded difference matrix
    mean_Amatrix_allSubjects_LR_display = mean_Amatrix_allSubjects_LR .* (pVal_Amatrix_allSubjects_LR <= threshold);
    
    % get the top connections (highest T values)
    if ( p_corr == 2 )
        mean_Amatrix_allSubjects_LR_display_binary = mean_Amatrix_allSubjects_LR_display == 0;
        mean_Amatrix_allSubjects_LR_top            = top_Amatrix_allSubjects_LR;
        mean_Amatrix_allSubjects_LR_top            = mean_Amatrix_allSubjects_LR_top - diag(diag(mean_Amatrix_allSubjects_LR_top));
        mean_Amatrix_allSubjects_LR_top            = mean_Amatrix_allSubjects_LR_top + (top_Amatrix_allSubjects_LR .* diag(diag(mean_Amatrix_allSubjects_LR_display ~= 0)));
    end
else
    mean_Amatrix_allSubjects_LR_display = mean_Amatrix_allSubjects_LR .* (pVal_Amatrix_allSubjects_LR < threshold);
end

% create the connectogram
mean_Amatrix_allSubjects_LR_store   = mean_Amatrix_allSubjects_LR_display;
mean_Amatrix_allSubjects_LR_display = abs(mean_Amatrix_allSubjects_LR_display-diag(diag(mean_Amatrix_allSubjects_LR_display)));
mean_Amatrix_allSubjects_LR_display = [[mean_Amatrix_allSubjects_LR_display(2:2:end,2:2:end),mean_Amatrix_allSubjects_LR_display(2:2:end,1:2:end)];
                                        [mean_Amatrix_allSubjects_LR_display(1:2:end,2:2:end),mean_Amatrix_allSubjects_LR_display(1:2:end,1:2:end)]];

% store the connectogram
if ( ~isempty(mean_Amatrix_allSubjects_LR_store) )
    
    % create folder
    if ( ~exist(fullfile(exp_foldername,aspirin_folder{aspirin+1},'DCM_GroupAnalysis',parcellations{parcellation_type},['sparse_regressionDCM' scale_name{scale+1}], input_name{restrictInputs+1}),'dir') )
        mkdir(fullfile(exp_foldername,aspirin_folder{aspirin+1},'DCM_GroupAnalysis',parcellations{parcellation_type},['sparse_regressionDCM' scale_name{scale+1}], input_name{restrictInputs+1}))
    end
    
    % store the connectogram
    save(fullfile(exp_foldername,aspirin_folder{aspirin+1},'DCM_GroupAnalysis',parcellations{parcellation_type},['sparse_regressionDCM' scale_name{scale+1}], input_name{restrictInputs+1}, ['Model' num2str(model) '_Difference_LR' pcorr_name{p_corr+1} '.mat']),'mean_Amatrix_allSubjects_LR_store')
end


% make directory for Circos
if ( ~exist(fullfile(results_folder, 'Circos'),'dir') )
    mkdir(fullfile(results_folder, 'Circos'))
end

% set the filename for the link files
filename_CIRCOS = fullfile(results_folder, 'Circos', ['Model' num2str(model) '_Difference_LR' pcorr_name{p_corr+1} '.txt']);

% option for circos file
circos_opt.scaling   = 16;
circos_opt.col       = {'255,0,0,1','44,162,95,1';'252,187,161,0.7','152,216,201,0.7'};
circos_opt.normalize = 'relative';
circos_opt.trans_con = mean_Amatrix_allSubjects_LR_display_binary;

% create the Circos link file
generate_links_circos_MainExperiment(mean_Amatrix_allSubjects_LR_top,parcellation_type,filename_CIRCOS,circos_opt)


% define the folder for the BrainNet files
folder_BrainNet   = fullfile(results_folder,'BrainNet');
filename_BrainNet = ['Model' num2str(model) '_Difference_LR' pcorr_name{p_corr+1}]; 

% create the BrainNet node and edge files
create_BrainNet_files_MainExperiment(mean_Amatrix_allSubjects_LR_store,folder_BrainNet,filename_BrainNet)

                                                 
% display the connectogram (using circularGraph.m) - NOTE: function does not work on Euler
if ( ~onCluster )
    circularGraph(mean_Amatrix_allSubjects_LR_display,'Label',strrep([temp.results.AllRegions(2:2:end), temp.results.AllRegions(1:2:end)],'_','\_'));
    print(gcf, '-dpsc', fullfile(results_folder,['Model' num2str(model) '_Difference_LR']), '-append')
end

end