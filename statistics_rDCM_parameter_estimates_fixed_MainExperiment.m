function statistics_rDCM_parameter_estimates_fixed_MainExperiment(model,aspirin,handedness,parcellation_type,p_corr,computeGT,onCluster)
% Summarize the individual parameter estimates from the whole-brain Dynamic 
% Causal Models (DCMs) for the hand movement dataset. Parameter estimates 
% have been computed using regression DCM (rDCM).
% 
% This function reads the summary file containing all the DCM parameter 
% estimates that have been estimated using regression DCM. The function 
% stores the individual parameter estimates for the effective connectivity 
% patterns that have been inferred under a fixed connectivity architecture.
% 
% Input:
%   model               -- model to analyze
%   aspirin             -- (0) no aspirin and (1) aspirin
% 	handedness         	-- left- or right-hand movement dataset
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainnetome 2016, (4) Glasser 2016 & Buckner 2011
%   p_corr              -- mutliple comparison correction: (1) FDR, (2) uncorrected
%   computeGT           -- (0) don't or (1) compute graph theoretical measures
%   onCluster           -- operating system: (1) Euler, (0) local machine
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% close all old figures
close all

% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the experiment folders
aspirin_folder    = {'wo_Aspirin/','Aspirin/','all_Aspirin/'};
handedness_folder = {'left','right'};

% set the foldername
exp_foldername  = FilenameInfo.DataPath;

% add the relevant paths 
addpath(fullfile(FilenameInfo.MATLAB_path,'fdr_bh'))
addpath(fullfile(FilenameInfo.MATLAB_path,'bipolar_colormap'))
addpath(genpath(fullfile(FilenameInfo.MATLAB_path,'cbrewer')))
addpath(fullfile(FilenameInfo.MATLAB_path,'circularGraph'))


% scale fMRI data (default: 0)
scale = 0;

% define the different names
scale_name = {'_noScale',''};

% set the parcellation
parcellations = {'Glasser2016','AAL','Brainnetome2016','Glasser2016_Buckner2011'};


% close all figures
close all


% display progress
disp('Loading data...')


if ( ~isempty(aspirin) )
    
    % set the SPM path and the path to the experiment
    foldername = [exp_foldername aspirin_folder{aspirin+1}];
    
    % load the results file
    temp = load(fullfile(foldername,'DCM_GroupAnalysis',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome'], ['PosteriorParameterEstimates_model' num2str(model) '.mat']));

    % asign the data
    Amatrix_allSubjects     = temp.results.A_Matrix_AllSubjects(:,:,handedness);
    Cmatrix_allSubjects     = temp.results.C_Matrix_AllSubjects(:,:,handedness);
    
else
    
    % iterate over both conditions
    for aspirin = [0 1]
        
        % set the SPM path and the path to the experiment
        foldername = [exp_foldername aspirin_folder{aspirin+1}];

        % load the results file
        temp = load(fullfile(foldername,'DCM_GroupAnalysis',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome'], ['PosteriorParameterEstimates_model' num2str(model) '.mat']));

        % asign the data
        if ( aspirin == 0 )
            Amatrix_allSubjects     = temp.results.A_Matrix_AllSubjects(:,:,handedness);
            Cmatrix_allSubjects     = temp.results.C_Matrix_AllSubjects(:,:,handedness);
        else
            for int = 1:size(temp.results.A_Matrix_AllSubjects,1)
                for int2 = 1:size(temp.results.A_Matrix_AllSubjects,2)
                    Amatrix_allSubjects{int,int2}  = [Amatrix_allSubjects{int,int2}; temp.results.A_Matrix_AllSubjects{int,int2,handedness}];
                end
                
                for int2 = 1:size(temp.results.C_Matrix_AllSubjects,2)
                    Cmatrix_allSubjects{int,int2}  = [Cmatrix_allSubjects{int,int2}; temp.results.C_Matrix_AllSubjects{int,int2,handedness}];
                end
            end
        end
    end
    
    % set aspirin condition to all subjects
    aspirin     = 2;
    
end


% specify an indicator vector
if ( aspirin == 0 )
    vector = [1:7 10:size(Amatrix_allSubjects{1,1},1)];
elseif ( aspirin == 1 )
    vector = [1:4 7:size(Amatrix_allSubjects{1,1},1)];
elseif ( aspirin == 2 )
    vector = [[1:7 10:14] [1:4 7:15]+14];
end


% dsiplay progress
disp(['# Subjects found: ' num2str(length(vector))])
disp('Computing summary statistics...')

% results arrays
mean_Amatrix_allSubjects = NaN(size(Amatrix_allSubjects));
mean_Cmatrix_allSubjects = NaN(size(Cmatrix_allSubjects));
per_Amatrix_allSubjects  = NaN(size(Amatrix_allSubjects));
per_Cmatrix_allSubjects  = NaN(size(Cmatrix_allSubjects));
pVal_Amatrix_allSubjects = NaN(size(Amatrix_allSubjects));
pVal_Cmatrix_allSubjects = NaN(size(Cmatrix_allSubjects));
Amatrix_allSubjects_Asym = NaN(size(Amatrix_allSubjects,1),size(Amatrix_allSubjects,2),length(vector));
Amatrix_allSubjects_Val  = NaN(size(Amatrix_allSubjects,1),size(Amatrix_allSubjects,2),length(vector));
Amatrix_allSubjects_temp = NaN(size(Amatrix_allSubjects,1),size(Amatrix_allSubjects,2),length(vector));
Cmatrix_allSubjects_temp = NaN(size(Cmatrix_allSubjects,1),size(Cmatrix_allSubjects,2),length(vector));

% get summaries for A matrix
for int = 1:size(Amatrix_allSubjects,1)
    for int2 = 1:size(Amatrix_allSubjects,2)
        
        % compute the mean endogenous connectivity
        mean_Amatrix_allSubjects(int,int2) = nanmean(Amatrix_allSubjects{int,int2}(vector));
        
        % compute asymmetries of the endogenous connectivity pattern
        Amatrix_allSubjects_Asym(int,int2,:) = Amatrix_allSubjects{int,int2}(vector) - Amatrix_allSubjects{int2,int}(vector);
        Amatrix_allSubjects_Val(int,int2,:)  = Amatrix_allSubjects{int,int2}(vector);
        
        % compute the percentage present for each connection
        per_Amatrix_allSubjects(int,int2) = nansum(Amatrix_allSubjects{int,int2}(vector)~=0)/length(vector);
        
        % create complete array
        for s = 1:length(vector)
            Amatrix_allSubjects_temp(int,int2,s) = Amatrix_allSubjects{int,int2}(vector(s));
        end
        
        % check for a significant difference
        [~,P,~,~] = ttest(Amatrix_allSubjects{int,int2}(vector),0);
        pVal_Amatrix_allSubjects(int,int2) = P;
        
    end
end


% get summaries for C matrix
for int = 1:size(Cmatrix_allSubjects,1)
    for int2 = 1:size(Cmatrix_allSubjects,2)
        
        % compute the mean driving input connectivity
        mean_Cmatrix_allSubjects(int,int2) = nanmean(Cmatrix_allSubjects{int,int2}(vector));
        
        % compute the percentage present for each connection
        per_Cmatrix_allSubjects(int,int2) = nansum(Cmatrix_allSubjects{int,int2}(vector)~=0)/length(vector);
        
        % create complete array
        for s = 1:length(vector)
            Cmatrix_allSubjects_temp(int,int2,s) = Cmatrix_allSubjects{int,int2}(vector(s));
        end
        
        % check for a significant difference
        [~,P,~,~] = ttest(Cmatrix_allSubjects{int,int2}(vector),0);
        pVal_Cmatrix_allSubjects(int,int2) = P;
        
    end
end


% specify the folder where results should be stored
results_folder      = fullfile(exp_foldername,'Figures','IBS',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome'],'Hand',aspirin_folder{aspirin+1});
results_folder_FC   = fullfile(exp_foldername,'Figures','IBS',parcellations{parcellation_type},['FC' scale_name{scale+1}],'Hand',aspirin_folder{aspirin+1});

% create the directory
if ( ~exist(results_folder,'dir') ) 
    mkdir(results_folder)
else
    if ( exist(fullfile(results_folder,['Model' num2str(model) '_' handedness_folder{handedness} 'Hand.ps']),'file') )
        delete(fullfile(results_folder,['Model' num2str(model) '_' handedness_folder{handedness} 'Hand.ps']))
    end
end

if ( ~exist(results_folder_FC,'dir') ) 
    mkdir(results_folder_FC)
else
    if ( exist(fullfile(results_folder_FC,['CrossCorr_' handedness_folder{handedness} 'Hand.ps']),'file') )
        delete(fullfile(results_folder_FC,['CrossCorr_' handedness_folder{handedness} 'Hand.ps']))
    end
end


% open the figures
figure('units','normalized','outerposition',[0 0 1 1])
figure('units','normalized','outerposition',[0 0 1 1])
figure('units','normalized','outerposition',[0 0 1 1])
figure('units','normalized','outerposition',[0 0 1 1])
figure('units','normalized','outerposition',[0 0 1 1])
figure('units','normalized','outerposition',[0 0 1 1])
figure('units','normalized','outerposition',[0 0 1 1])
figure('units','normalized','outerposition',[0 0 1 1])
figure('units','normalized','outerposition',[0 0 1 1])
figure('units','normalized','outerposition',[0 0 1 1])
figure('units','normalized','outerposition',[0 0 1 1])


% reorient matrix 
mean_Amatrix_allSubjects_reorder    = reorder_adjacency_matrix_MainExperiment(mean_Amatrix_allSubjects);
per_Amatrix_allSubjects_reorder     = reorder_adjacency_matrix_MainExperiment(per_Amatrix_allSubjects);
mean_Cmatrix_allSubjects_reorder    = reorder_adjacency_matrix_MainExperiment(mean_Cmatrix_allSubjects);
per_Cmatrix_allSubjects_reorder     = reorder_adjacency_matrix_MainExperiment(per_Cmatrix_allSubjects);


% display the mean endogenous connectivity
figure(1)
h = gcf;
imagesc(mean_Amatrix_allSubjects_reorder)
hold on
plot([0 size(mean_Amatrix_allSubjects_reorder,2)+0.5],[size(mean_Amatrix_allSubjects_reorder,1)/2+0.5 size(mean_Amatrix_allSubjects_reorder,1)/2+0.5],'k-')
plot([size(mean_Amatrix_allSubjects_reorder,2)/2+0.5 size(mean_Amatrix_allSubjects_reorder,2)/2+0.5],[0 size(mean_Amatrix_allSubjects_reorder,1)+0.5],'k-')
colormap('bipolar')
caxis([-0.01 0.01])
axis square
colorbar
h.CurrentAxes.FontSize = 18;
title(['mean A matrix - ' handedness_folder{handedness} ' hand'],'FontSize',20)
xlabel('region (from)','FontSize',20)
ylabel('region (to)','FontSize',20)
set(gca,'xtick',[1 size(mean_Amatrix_allSubjects_reorder,1)/2 size(mean_Amatrix_allSubjects_reorder,1)])
set(gca,'ytick',[1 size(mean_Amatrix_allSubjects_reorder,1)/2 size(mean_Amatrix_allSubjects_reorder,1)])
print(gcf, '-dpsc', fullfile(results_folder,['Model' num2str(model) '_' handedness_folder{handedness} 'Hand.ps']), '-append')

figure(2)
h = gcf;
imagesc(per_Amatrix_allSubjects_reorder);
colormap('gray')
caxis([0 floor(max(max(per_Amatrix_allSubjects_reorder-diag(diag(per_Amatrix_allSubjects_reorder))))*10)/10])
hold on
plot([0 size(per_Amatrix_allSubjects_reorder,2)+0.5],[size(per_Amatrix_allSubjects_reorder,1)/2+0.5 size(per_Amatrix_allSubjects_reorder,1)/2+0.5],'k-')
plot([size(per_Amatrix_allSubjects_reorder,2)/2+0.5 size(per_Amatrix_allSubjects_reorder,2)/2+0.5],[0 size(per_Amatrix_allSubjects_reorder,1)+0.5],'k-')
axis square
colorbar
h.CurrentAxes.FontSize = 18;
title(['percentage A matrix - ' handedness_folder{handedness} ' hand'],'FontSize',20)
xlabel('region (from)','FontSize',20)
ylabel('region (to)','FontSize',20)
set(gca,'xtick',[1 size(per_Amatrix_allSubjects_reorder,1)/2 size(per_Amatrix_allSubjects_reorder,1)])
set(gca,'ytick',[1 size(per_Amatrix_allSubjects_reorder,1)/2 size(per_Amatrix_allSubjects_reorder,1)])
print(gcf, '-dpsc', fullfile(results_folder,['Model' num2str(model) '_' handedness_folder{handedness} 'Hand.ps']), '-append')


% display the asymmetry of the mean endogenous connectivity
figure(3)
h = gcf;
imagesc(mean_Amatrix_allSubjects_reorder-mean_Amatrix_allSubjects_reorder')
hold on
plot([0 size(mean_Amatrix_allSubjects_reorder,2)+0.5],[size(mean_Amatrix_allSubjects_reorder,1)/2+0.5 size(mean_Amatrix_allSubjects_reorder,1)/2+0.5],'k-')
plot([size(mean_Amatrix_allSubjects_reorder,2)/2+0.5 size(mean_Amatrix_allSubjects_reorder,2)/2+0.5],[0 size(mean_Amatrix_allSubjects_reorder,1)+0.5],'k-')
colormap(bipolar)
caxis([-0.01 0.01])
axis square
colorbar
h.CurrentAxes.FontSize = 18;
title(['asymmetry mean A matrix - ' handedness_folder{handedness} ' hand'],'FontSize',20)
xlabel('region (from)','FontSize',20)
ylabel('region (to)','FontSize',20)
set(gca,'xtick',[1 size(mean_Amatrix_allSubjects_reorder,1)/2 size(mean_Amatrix_allSubjects_reorder,1)])
set(gca,'ytick',[1 size(mean_Amatrix_allSubjects_reorder,1)/2 size(mean_Amatrix_allSubjects_reorder,1)])
print(gcf, '-dpsc', fullfile(results_folder,['Model' num2str(model) '_' handedness_folder{handedness} 'Hand.ps']), '-append')


% display histogram of the asymmetry of the mean endogenous connectivity
Amatrix_allSubjects_Asym = Amatrix_allSubjects_Asym(Amatrix_allSubjects_Asym~=0);
Amatrix_allSubjects_Val = Amatrix_allSubjects_Val(Amatrix_allSubjects_Val~=0);
figure(4)
h = gcf;
[N,X]   = hist(Amatrix_allSubjects_Asym,-1:0.001:1);
[N2,X2] = hist(Amatrix_allSubjects_Val,-1:0.001:1);
ha(2) = bar(X2,N2,'EdgeColor','k','FaceColor','r');
hold on
ha(1) = bar(X+0.0001,N,'EdgeColor','k','FaceColor','w');
axis square
box off
set(gca,'TickDir','out');
h.CurrentAxes.FontSize = 18;
title(['histogram - asymmetry mean A matrix - ' handedness_folder{handedness} ' hand'],'FontSize',20)
xlabel('degree of asymmetry (mean A matrix)','FontSize',20)
ylabel('number #','FontSize',20)
xlim([-0.03 0.03])
legend(ha,{'asymmetry','connection strength'},'Location','NW')
legend boxoff
print(gcf, '-dpsc', fullfile(results_folder,['Model' num2str(model) '_' handedness_folder{handedness} 'Hand.ps']), '-append')


% display the mean driving input connectivity
figure(5);
h = gcf;
imagesc(mean_Cmatrix_allSubjects_reorder);
hold on
plot([0 size(mean_Cmatrix_allSubjects_reorder,2)+0.5],[size(mean_Cmatrix_allSubjects_reorder,1)/2+0.5 size(mean_Cmatrix_allSubjects_reorder,1)/2+0.5],'k-')
colormap('bipolar')
caxis([-1*floor(max(max(abs(mean_Cmatrix_allSubjects)))*10)/10 floor(max(max(abs(mean_Cmatrix_allSubjects)))*10)/10])
axis square
colorbar
h.CurrentAxes.FontSize = 18;
set(gca,'xtick',1)
set(gca,'xticklabel','')
title(['mean C matrix - ' handedness_folder{handedness} ' hand'],'FontSize',20)
xlabel('driving input','FontSize',20)
ylabel('region (to)','FontSize',20)
set(gca,'ytick',[1 size(mean_Cmatrix_allSubjects,1)/2 size(mean_Cmatrix_allSubjects,1)])

% get the top most driving inputs
[~,I] = sort(abs(mean_Cmatrix_allSubjects),'descend');
for int = 1:10
    text(0.5+(mod(int,5)*0.18),I(int),strrep(temp.results.AllRegions{I(int)},'_','\_'),'FontSize',14,'FontWeight','bold')
end
print(gcf, '-dpsc', fullfile(results_folder,['Model' num2str(model) '_' handedness_folder{handedness} 'Hand.ps']), '-append')


figure(6);
h = gcf;
imagesc(per_Cmatrix_allSubjects_reorder);
hold on
plot([0 size(per_Cmatrix_allSubjects_reorder,2)+0.5],[size(per_Cmatrix_allSubjects_reorder,1)/2+0.5 size(per_Cmatrix_allSubjects_reorder,1)/2+0.5],'k-')
colormap('gray')
caxis([0 floor(max(max(per_Cmatrix_allSubjects))*10)/10])
axis square
colorbar
h.CurrentAxes.FontSize = 18;
set(gca,'xtick',1)
set(gca,'xticklabel','')
title(['percentage C matrix - ' handedness_folder{handedness} ' hand'],'FontSize',20)
xlabel('driving input','FontSize',20)
ylabel('region (to)','FontSize',20)
set(gca,'ytick',[1 size(per_Cmatrix_allSubjects,1)/2 size(per_Cmatrix_allSubjects,1)])
print(gcf, '-dpsc', fullfile(results_folder,['Model' num2str(model) '_' handedness_folder{handedness} 'Hand.ps']), '-append')


% specify the number of parameters
NRparam_A   = NaN(size(Amatrix_allSubjects_temp,3),1);
NRparam_C   = NaN(size(Amatrix_allSubjects_temp,3),1);
NRparam     = NaN(size(Amatrix_allSubjects_temp,3),1);

% compute the number of parameters
for s = 1:size(Amatrix_allSubjects_temp,3)
    NRparam_A(s) = sum(sum(Amatrix_allSubjects_temp(:,:,s)~=0));
    NRparam_C(s) = sum(sum(Cmatrix_allSubjects_temp(:,:,s)~=0));
    NRparam(s)   = NRparam_A(s) + NRparam_C(s); 
end

% get the number of connections present in at least one subejct
temp_matrix	 = nansum(Amatrix_allSubjects_temp~=0,3);
NRparam_once = nansum(temp_matrix(:)>0);


% set the options
options = vbsv_graphics_options();


% plot the number of parameters
figure(7);
hold on

for cat = 1:3
    
    switch cat
        case 1
            NRparam_plot = NRparam_A;
        case 2
            NRparam_plot = NRparam_C;
        case 3
            NRparam_plot = NRparam;
    end
    
    for int = 1:length(NRparam_plot)
        h = plot(cat,NRparam_plot(int),'o');
        h.MarkerSize      = options.Ep.Marker.normal.size;
        h.MarkerFaceColor = options.Ep.Marker.normal.FaceColor;
        h.MarkerEdgeColor = options.Ep.Marker.normal.EdgeColor;
    end
    
    h = plot(cat,mean(NRparam_plot),'o');
    h.Marker          = options.top.Marker{1};
    h.MarkerSize      = options.Ep.Marker.top.size(1);
    h.MarkerFaceColor = options.Ep.Marker.default.FaceColor;
    h.MarkerEdgeColor = options.Ep.Marker.default.EdgeColor;

    switch cat
        case 1
            text(0.5,min(NRparam_plot),num2str(min(NRparam_plot)))
            text(0.5,max(NRparam_plot),num2str(max(NRparam_plot)))
        case 3
            h = plot(cat,NRparam_once,'o');
            h.Marker          = options.top.Marker{2};
            h.MarkerSize      = options.Ep.Marker.top.size(2);
            h.MarkerFaceColor = [0 0.9 0];
            h.MarkerEdgeColor = options.Ep.Marker.default.EdgeColor;
    end

    axis square
    xlim([0 4])
    set(gca,'xtick',[1 2 3])
    set(gca,'xticklabel',{'A (endogenous)','C (driving)','all'},'FontSize',11)
    ylabel('# parameters','FontSize',11)
    title(['Number of parameters - ' handedness_folder{handedness} ' hand'],'FontSize',12)

end

% store figure
print(gcf, '-dpsc', fullfile(results_folder,['Model' num2str(model) '_' handedness_folder{handedness} 'Hand.ps']), '-append')


% open figure
figure(8);

% set the threshold
if ( p_corr == 1 )
    p_all             = [pVal_Amatrix_allSubjects(:); pVal_Cmatrix_allSubjects(:)];
    p_all             = p_all(isfinite(p_all));
    [~, crit_p, ~, ~] = fdr_bh(p_all,0.05,'dep','no');
    threshold         = crit_p;
    disp('')
    disp(['Estimated p-Value (FDR-corrected): ' num2str(threshold)])
else
    threshold = 0.001;
    disp('')
    disp(['Estimated p-Value (uncorrected): ' num2str(threshold)])
end

% create the connectogram
if ( p_corr == 1 )
    mean_Amatrix_allSubjects_display = mean_Amatrix_allSubjects .* (pVal_Amatrix_allSubjects <= threshold);
else
    mean_Amatrix_allSubjects_display = mean_Amatrix_allSubjects .* (pVal_Amatrix_allSubjects < threshold);
end


% get the mean connectivity strengths and threshold
mean_Amatrix_allSubjects_store   = mean_Amatrix_allSubjects_display;
mean_Amatrix_allSubjects_store   = mean_Amatrix_allSubjects_store-diag(diag(mean_Amatrix_allSubjects_store));
mean_Amatrix_allSubjects_display = abs(mean_Amatrix_allSubjects_display-diag(diag(mean_Amatrix_allSubjects_display)));


% arrange the matrix accoringly
mean_Amatrix_allSubjects_display = [[mean_Amatrix_allSubjects_display(2:2:end,2:2:end),mean_Amatrix_allSubjects_display(2:2:end,1:2:end)];
                                        [mean_Amatrix_allSubjects_display(1:2:end,2:2:end),mean_Amatrix_allSubjects_display(1:2:end,1:2:end)]];

% display the connectogram (using circularGraph.m) - not supported on Euler
if ( ~onCluster )
    circularGraph(mean_Amatrix_allSubjects_display,'Label',strrep([temp.results.AllRegions(2:2:end), temp.results.AllRegions(1:2:end)],'_','\_'));
    print(gcf, '-dpsc', fullfile(results_folder,['Model' num2str(model) '_' handedness_folder{handedness} 'Hand.ps']), '-append')
end


% load the functional connectivity files
if ( aspirin ~= 2 )
    
    % set the SPM path and the path to the experiment
    foldername = [exp_foldername aspirin_folder{aspirin+1}];
    
    % load the results file
    temp_FC = load(fullfile(foldername,'FC_GroupAnalysis',['tapping_' handedness_folder{handedness}],parcellations{parcellation_type},'FC_measures.mat'));
    
    % asign the data
    Corr_AllSubjects = temp_FC.FC_measures.fullCorr_AllSubjects;
    
else
    
    % iterate over both conditions
    for aspirin = [0 1]
        
        % set the SPM path and the path to the experiment
        foldername = [exp_foldername aspirin_folder{aspirin+1}];
        
        % load the results file
        temp_FC = load(fullfile(foldername,'FC_GroupAnalysis',['tapping_' handedness_folder{handedness}],parcellations{parcellation_type},'FC_measures.mat'));
        
        % asign the data (temp)
        Corr_AllSubjects_temp = temp_FC.FC_measures.fullCorr_AllSubjects;
        
        % asign the data
        if ( aspirin == 0 )
            Corr_AllSubjects = Corr_AllSubjects_temp;
        else
            for int = 1:size(Corr_AllSubjects_temp,1)
                for int2 = 1:size(Corr_AllSubjects_temp,2)
                    Corr_AllSubjects{int,int2}  = [Corr_AllSubjects{int,int2}; Corr_AllSubjects_temp{int,int2}];
                end
            end
        end
    end
    
    % set aspirin to 2 again
    aspirin = 2;
    
end


% create array
mean_fullCorr_AllSubjects = NaN(size(Corr_AllSubjects));
fullCorr_allSubjects_Asym = NaN(size(Corr_AllSubjects,1),size(Corr_AllSubjects,2),length(vector));
fullCorr_allSubjects_Val  = NaN(size(Corr_AllSubjects,1),size(Corr_AllSubjects,2),length(vector));


% get the mean functional connectivity
for int = 1:size(Corr_AllSubjects,1)
    for int2 = 1:size(Corr_AllSubjects,2)
        
        % compute the mean functional connectivity
        mean_fullCorr_AllSubjects(int,int2) = nanmean(Corr_AllSubjects{int,int2}(vector));
        
        % compute asymmetries of the endogenous connectivity pattern
        if ( int == int2 )
            fullCorr_allSubjects_Asym(int,int2,:) = NaN;
        else
            fullCorr_allSubjects_Asym(int,int2,:) = Corr_AllSubjects{int,int2}(vector) - Corr_AllSubjects{int2,int}(vector);
        end
        
        % store the value of the functional connectivity estimates
        fullCorr_allSubjects_Val(int,int2,:)  = Corr_AllSubjects{int,int2}(vector);
        
    end
end


% get the threshold to ensure equal sparsity of the matrices
sorted_mean_fullCorr_AllSubjects = sort(abs(mean_fullCorr_AllSubjects(:)),'descend');
threshold_fullCorr               = sorted_mean_fullCorr_AllSubjects(sum(per_Amatrix_allSubjects(:)));

% reoder the functional connectivity matrix
mean_fullCorr_AllSubjects_reorder   = reorder_adjacency_matrix_MainExperiment(mean_fullCorr_AllSubjects);

% plot average functional connectivity
figure(9)
imagesc(mean_fullCorr_AllSubjects_reorder.*(abs(mean_fullCorr_AllSubjects_reorder) >= threshold_fullCorr));
hold on
plot([0 size(mean_fullCorr_AllSubjects_reorder,2)+0.5],[size(mean_fullCorr_AllSubjects_reorder,1)/2+0.5 size(mean_fullCorr_AllSubjects_reorder,1)/2+0.5],'k-')
plot([size(mean_fullCorr_AllSubjects_reorder,2)/2+0.5 size(mean_fullCorr_AllSubjects_reorder,2)/2+0.5],[0 size(mean_fullCorr_AllSubjects_reorder,1)+0.5],'k-')
colormap('bipolar')
caxis([-0.8 0.8])
axis square
colorbar
title('mean functional connectivity (Pearson)','FontSize',12)
xlabel('region (from)','FontSize',11)
ylabel('region (to)','FontSize',11)
set(gca,'xtick',[1 size(mean_fullCorr_AllSubjects_reorder,1)/2 size(mean_fullCorr_AllSubjects_reorder,1)])
set(gca,'ytick',[1 size(mean_fullCorr_AllSubjects_reorder,1)/2 size(mean_fullCorr_AllSubjects_reorder,1)])
print(gcf, '-dpsc', fullfile(results_folder_FC,['CrossCorr_' handedness_folder{handedness} 'Hand.ps']), '-append')

% plot the present connections (at the sparsity of the Brainnetome atlas)
figure(10)
imagesc(abs(mean_fullCorr_AllSubjects_reorder) >= threshold_fullCorr);
hold on
plot([0 size(mean_fullCorr_AllSubjects_reorder,2)+0.5],[size(mean_fullCorr_AllSubjects_reorder,1)/2+0.5 size(mean_fullCorr_AllSubjects_reorder,1)/2+0.5],'k-')
plot([size(mean_fullCorr_AllSubjects_reorder,2)/2+0.5 size(mean_fullCorr_AllSubjects_reorder,2)/2+0.5],[0 size(mean_fullCorr_AllSubjects_reorder,1)+0.5],'k-')
colormap('gray')
caxis([0 1])
axis square
colorbar
title('significant functional connectivity (Pearson)','FontSize',12)
xlabel('region (from)','FontSize',11)
ylabel('region (to)','FontSize',11)
set(gca,'xtick',[1 size(mean_fullCorr_AllSubjects_reorder,1)/2 size(mean_fullCorr_AllSubjects_reorder,1)])
set(gca,'ytick',[1 size(mean_fullCorr_AllSubjects_reorder,1)/2 size(mean_fullCorr_AllSubjects_reorder,1)])
print(gcf, '-dpsc', fullfile(results_folder_FC,['CrossCorr_' handedness_folder{handedness} 'Hand.ps']), '-append')


% display histogram of the asymmetry of the mean endogenous connectivity
fullCorr_allSubjects_Asym = fullCorr_allSubjects_Asym(isfinite(fullCorr_allSubjects_Asym));
fullCorr_allSubjects_Val  = fullCorr_allSubjects_Val(fullCorr_allSubjects_Val~=1);
figure(11)
h = gcf;
[N,X]   = hist(fullCorr_allSubjects_Asym,-1:0.02:1);
[N2,X2] = hist(fullCorr_allSubjects_Val,-1:0.02:1);
ha(2) = bar(X2,N2,'EdgeColor','k','FaceColor','r');
hold on
ha(1) = bar(X+0.0001,N,'EdgeColor','k','FaceColor','w');
axis square
box off
set(gca,'TickDir','out');
h.CurrentAxes.FontSize = 18;
title(['histogram - asymmetry functional connectivity - ' handedness_folder{handedness} ' hand'],'FontSize',20)
xlabel('degree of asymmetry (functional connectivity)','FontSize',20)
ylabel('number #','FontSize',20)
xlim([-1 1])
ylim([0 100000])
legend(ha,{'asymmetry','connection strength'},'Location','NW')
legend boxoff
print(gcf, '-dpsc', fullfile(results_folder_FC,['CrossCorr_' handedness_folder{handedness} 'Hand.ps']), '-append')


% make directory for Circos
if ( ~exist(fullfile(results_folder,'Circos'),'dir') )
    mkdir(fullfile(results_folder,'Circos'))
end

% set the filename for the link files
filename_CIRCOS = fullfile(results_folder,'Circos',['Model' num2str(model) '_' handedness_folder{handedness} 'Hand.txt']);

% option for circos file
circos_opt.scaling   = 16;
circos_opt.col       = {'255,0,0,1','44,162,95,1'};
circos_opt.normalize = 'relative';

% create the Circos link file
generate_links_circos_MainExperiment(mean_Amatrix_allSubjects_store,parcellation_type,filename_CIRCOS,circos_opt)



% make directory for BrainNet
if ( ~exist(fullfile(results_folder,'BrainNet'),'dir') )
    mkdir(fullfile(results_folder,'BrainNet'))
end


% define the folder for the BrainNet files
folder_BrainNet   = fullfile(results_folder,'BrainNet');
filename_BrainNet = ['/Model' num2str(model) '_' handedness_folder{handedness} '_Hand']; 

% create the BrainNet node and edge files
create_BrainNet_files_MainExperiment(mean_Amatrix_allSubjects_store,folder_BrainNet,filename_BrainNet)


% compute graph theory measures
if ( computeGT )
    
    fprintf('\nComputing graph theoretical measures...\n')
    
    % compute graph theoretical measures
    for s = 1:size(Amatrix_allSubjects_temp,3)
        
        % remove the diagonal
        Amatrix_temp = Amatrix_allSubjects_temp(:,:,s)-diag(diag(Amatrix_allSubjects_temp(:,:,s)));
        
        % transpose A-matrix (as Brain Connectivity toolbox uses switched notation)
        Amatrix_temp = Amatrix_temp';
        
        % compute graph theoretical measures
        if ( any(Amatrix_temp(:)~=0) && any(isfinite(Amatrix_temp(:))) )
            disp(['Subject #: ' num2str(vector(s))])
            results_temp = compute_graph_theoretical_measures(Amatrix_temp);
            results(s)   = results_temp;
        else
            disp(['Subject #: ' num2str(vector(s)) ' (ERROR)'])
        end
    end
    
    % set the SPM path and the path to the experiment
    foldername = [exp_foldername aspirin_folder{aspirin+1}];
    
    % store the graph theoretical measures
    save(fullfile(foldername,'DCM_GroupAnalysis',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome'], ['rDCM_GraphTheory_model' num2str(model) '_' handedness_folder{handedness} 'Hand.mat']), 'results')
    
end

end