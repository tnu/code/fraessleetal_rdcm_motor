function BMS_srDCM_MainExperiment(subject_analyze,aspirin,restrictInputs,parcellation_type)
% Compare two differen whole-brain Dynamic Causal Models (DCMs) for the 
% hand movement datasets of the Stroke patient study. Specifically, the
% function compares the model under fixed endogenous connections (as 
% informed by the Brainnetome atlas) and the model under sparsity constraints.
% 
% Input:
%   subject_analyze     -- subjects to include (default: empty)
%   aspirin             -- (0) no aspirin and (1) aspirin
%   restrictInputs      -- inputs: (0) all inputs, (1) restrict inputs
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainnetome 2016, (4) Glasser 2016 & Buckner 2011
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the experiment folders
aspirin_folder    = {'wo_Aspirin/','Aspirin/','all_Aspirin/'};
handedness_folder = {'left','right'};

% add the relevant paths and set the foldername
addpath(fullfile(FilenameInfo.SPM_path))
exp_foldername  = FilenameInfo.DataPath;


% define the different names
scale_name      = {'_noScale',''};
input_name      = {'allInput','restrictInputs'};

% set the parcellation
parcellations = {'Glasser2016','AAL','Brainnetome2016','Glasser2016_Buckner2011'};

% set to no scaling
scale = 0;

% close all figures
close all


% display progress
disp('Loading data...')


% initialize the 
F_allSubjects = cell(length(handedness_folder),1);


for handedness = 1:length(handedness_folder)
    if ( ~isempty(aspirin) )

        % set the SPM path and the path to the experiment
        foldername = fullfile(exp_foldername, aspirin_folder{aspirin+1});

        % load the results file
        temp = load(fullfile(foldername,'DCM_GroupAnalysis',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome'], 'PosteriorParameterEstimates_model1.mat'));

        % asign the data
        F_allSubjects{handedness}(:,1) = temp.results.F_AllSubjects{handedness};

        % load the results file
        temp = load(fullfile(foldername,'DCM_GroupAnalysis',parcellations{parcellation_type},['sparse_regressionDCM' scale_name{scale+1}], input_name{restrictInputs+1}, 'PosteriorParameterEstimates_model1.mat'));

        % asign the data
        F_allSubjects{handedness}(:,2) = temp.results.F_AllSubjects{handedness};
        
    else

        % iterate over both conditions
        for aspirin_ind = [0 1]

            % set the SPM path and the path to the experiment
            foldername = [exp_foldername aspirin_folder{aspirin_ind+1}];

            % load the results file
            temp  = load(fullfile(foldername,'DCM_GroupAnalysis',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome'], 'PosteriorParameterEstimates_model1.mat'));
            temp2 = load(fullfile(foldername,'DCM_GroupAnalysis',parcellations{parcellation_type},['sparse_regressionDCM' scale_name{scale+1}], input_name{restrictInputs+1}, 'PosteriorParameterEstimates_model1.mat'));

            % asign the data
            if ( aspirin_ind == 0 )
                F_allSubjects_temp  = temp.results.F_AllSubjects{handedness};
                F_allSubjects_temp2 = temp2.results.F_AllSubjects{handedness};
            else
                F_allSubjects_temp  = [F_allSubjects_temp; temp.results.F_AllSubjects{handedness}];
                F_allSubjects_temp2 = [F_allSubjects_temp2; temp2.results.F_AllSubjects{handedness}];
            end
        end

        F_allSubjects{handedness}(:,1) = F_allSubjects_temp;
        F_allSubjects{handedness}(:,2) = F_allSubjects_temp2;
        
    end
end


% set aspirin condition to all subjects
if ( isempty(aspirin) )
    aspirin = 2;
end


% specify an indicator vector
if ( isempty(subject_analyze) )
    if ( aspirin == 0 )
        vector = [1:7 10:size(F_allSubjects{1},1)];
    elseif ( aspirin == 1 )
        vector = [1:4 7:size(F_allSubjects{1},1)];
    elseif ( aspirin == 2 )
        vector = [[1:7 10:14] [1:4 7:15]+14];
    end
else
    vector = subject_analyze;
end


% sanity check: any infinite or NaN values
for handedness = 1:2
    if ( any(~isfinite(F_allSubjects{handedness}(vector,:))) )
        disp(['NaN value detected for condition: ' handedness_folder{handedness} ' hand'])
        return
    end
end


% sum over sessions (left + right)
F_allSubjects_allSessions = F_allSubjects{1} + F_allSubjects{2};


% dsiplay progress
disp(['# Subjects found: ' num2str(length(vector))])


% asign the negative free energy array
F = F_allSubjects_allSessions(vector,:);

% M: number of models
M = size(F,2);

% call the random effects BMS function
[~, exp_r, ~, pxp, ~] = spm_BMS(F, 1e6, 1, 0, 1, ones(1,M));


% close all open figures
close all

% define the names
model_names = {'Brainnetome','Sparsity'};


% specify the folder where results should be stored
results_folder = fullfile(exp_foldername,'Figures','IBS',parcellations{parcellation_type},['sparse_regressionDCM' scale_name{scale+1}],input_name{restrictInputs+1},'Hand',aspirin_folder{aspirin+1});

% create the directory
if ( ~exist(results_folder,'dir') ) 
    mkdir(results_folder)
else
    if ( exist(fullfile(results_folder,'Model1_BMS_srDCM.ps'),'file') )
        delete(fullfile(results_folder,'Model1_BMS_srDCM.ps'))
    end
end


% Plot the expected probability and the protected exceedance probability
figure('units','normalized','outerposition',[0 0 1 1]);
col = [0.7 0.7 0.7];
bar(exp_r,'FaceColor',col);
xlim([0.25 size(pxp,2)+0.75])
title('BMS: Anatomical information vs. sparsity constraints','FontSize',20)
xlabel('Model / Connectivity','FontSize',18)
ylabel('expected posterior probability (exp)','FontSize',18)
set(gca,'FontSize',18);
set(gca,'ytick',[0 0.5 1])
set(gca,'xtick',1:2)
set(gca,'xticklabel',model_names)
box off
axis square
print(gcf, '-dpsc', fullfile(results_folder,'Model1_BMS_srDCM'), '-append')

figure('units','normalized','outerposition',[0 0 1 1]);
col = [0.7 0.7 0.7];
bar(pxp,'FaceColor',col);
xlim([0.25 size(pxp,2)+0.75])
title('BMS: Anatomical information vs. sparsity constraints','FontSize',20)
xlabel('Model / Connectivity','FontSize',18)
ylabel('protected exceedance probability (pxp)','FontSize',18)
set(gca,'FontSize',18);
set(gca,'ytick',[0 0.5 1])
set(gca,'xtick',1:2)
set(gca,'xticklabel',model_names)
box off
axis square
print(gcf, '-dpsc', fullfile(results_folder,'Model1_BMS_srDCM'), '-append')

end