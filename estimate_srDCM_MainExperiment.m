function estimate_srDCM_MainExperiment(subject,model,aspirin,handedness,parcellation_type,restrictInputs,p0_ind,parallelized)
% Infer whole-brain effective connectivity for the hand movement dataset
% using regression DCM.
% 
% This function prepares the respective DCM model. Futher, it applies rDCM 
% to the model, which means assuming a fully (all-to-all) connected model,
% which is then pruned during model inversion to yield a sparse
% representation of network connectivity.
% 
% Input:
%   subject             -- subject to analyze
%   model               -- model to analyze
%   aspirin             -- (0) no aspirin and (1) aspirin
% 	handedness         	-- left- or right-hand movement dataset
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainetome 2016, (4) Glasser 2016 & Buckner 2011
%   restrictInputs      -- inputs: (0) all inputs, (1) restrict inputs
%   p0_ind              -- prior on sparsity constraint
%   parallelized        -- parallelize inference: (1) yes, (0) no
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the experiment folders
aspirin_folder    = {'wo_Aspirin/','Aspirin/'};
handedness_folder = {'left','right'};


% add the relevant paths and set the foldername
pre_foldername = FilenameInfo.DataPath;
addpath(genpath(fullfile(FilenameInfo.TAPAS_path,'rDCM')))
addpath(FilenameInfo.SPM_path)


% define the different names
foldername        = fullfile(pre_foldername, aspirin_folder{aspirin+1});
scale_name        = {'_noScale',''};
input_name        = {'allInput','restrictInputs'};
parall_name       = {'','_parallelized'};

% define the filename
filename = ['DCM_hand_model' num2str(model)];


% define the standard option settings
options.scale      = 0;
options.estimateVL = 0;

% asign the scale
scale = options.scale;

% run parallelized code (default: no parallelization)
if ( isempty(parallelized) ), parallelized = 0; end


% get the subject list
Subject_List = dir(fullfile(foldername,'1*'));

% subject name
Subject = Subject_List(subject).name;

% display subject name
disp(['Subject: ' Subject])

% clear any DCM structure
clear DCM

% set the parcellation
parcellations     = {'Glasser2016','AAL','Brainnetome2016','Glasser2016_Buckner2011'};

% load the SPM
load(fullfile(foldername, Subject, 'FirstLevel', 'IBS', ['tapping_' handedness_folder{handedness}], 'SPM.mat'))

% asign the data of the respective subject to the DCM
VOI_foldername = fullfile(foldername, Subject, 'FirstLevel', 'IBS', ['tapping_' handedness_folder{handedness}], 'timeseries_VOI_adjusted', parcellations{parcellation_type});


% get the ROI names that are not present in all subjects
temp_ROI = load(fullfile(pre_foldername, 'VoI_coordinates', 'IBS', parcellations{parcellation_type}, 'MissingROIs.mat'));

% remove ROIs that are not present in all subjects
for ROI_int = 1:length(temp_ROI.allMissing_regions)
    if ( parcellation_type ~= 3 )
        delete(fullfile(VOI_foldername, ['VOI_' temp_ROI.allMissing_regions{ROI_int}(1:(find(temp_ROI.allMissing_regions{ROI_int}=='_',1,'last'))) '*.mat']))
    else
        delete(fullfile(VOI_foldername, ['VOI_*' temp_ROI.allMissing_regions{ROI_int}(5:(find(temp_ROI.allMissing_regions{ROI_int}=='_',1,'last'))) '*.mat']))
    end
end


% get the VOI file in the folder
VOI_files = dir(fullfile(VOI_foldername, 'VOI*.mat'));

% display progress
disp('Loading data...')
disp(['Found number of regions: ' num2str(length(VOI_files))])

% load the VOI time series for all regions of interes                                                                                                                            
for number_of_regions = 1:length(VOI_files)

    % load the files
    load(fullfile(VOI_foldername,VOI_files(number_of_regions).name),'xY');
    DCM.xY(number_of_regions) = xY;

end

% number of regions
DCM.n = length(DCM.xY);

% number of time points
DCM.v = length(DCM.xY(1).u);

% specify the TR
DCM.Y.dt  = SPM.xY.RT;

% specify the Y component of the DCM file
DCM.Y.X0 = DCM.xY(1).X0;

% asign the data to the Y structure
for i = 1:DCM.n
    DCM.Y.y(:,i)  = DCM.xY(i).u;
    DCM.Y.name{i} = DCM.xY(i).name;
end

% define the covariance matrix
DCM.Y.Q = spm_Ce(ones(1,DCM.n)*DCM.v);

% Experimental inputs
DCM.U.dt   = SPM.Sess.U(1).dt;
DCM.U.name = [SPM.Sess.U.name];
DCM.U.u    = SPM.Sess.U(1).u(33:end,1);

% DCM parameters
DCM.delays = repmat(SPM.xY.RT/2,DCM.n,1);
DCM.TE     = 0.0250;

% DCM options
DCM.options.nonlinear  = 0;
DCM.options.two_state  = 0;
DCM.options.stochastic = 0;
DCM.options.nograph    = 0;


% load the structural connectome
temp_adj         = load(fullfile(FilenameInfo.LongTermStorage_path, 'parcellation', parcellations{parcellation_type}, 'Adjacency_matrix.mat'));
adjacency_matrix = temp_adj.adjacency_matrix;

% get all regions where signal was detected
vector          = 1:size(adjacency_matrix,1);
include_regions = setdiff(vector,temp_ROI.allMissing_regions_ind);

% get the adjacency matrix
adjacency_matrix_cut = adjacency_matrix(include_regions,include_regions);
adjacency_matrix_cut = adjacency_matrix_cut - diag(diag(adjacency_matrix_cut));

% connectivity structure of respective model:
%   Model 1: Driving input to all regions
%   Model 2: Driving input to occipital and post-/precentral regions
if ( model == 1 )
    DCM.a = adjacency_matrix_cut + eye(DCM.n);
    DCM.b = zeros(DCM.n,DCM.n,size(DCM.U.u,2));
    DCM.c = ones(DCM.n,size(DCM.U.u,2));
    DCM.d = zeros(DCM.n,DCM.n,0);
elseif ( model == 2 )
    DCM.a = adjacency_matrix_cut + eye(DCM.n);
    DCM.b = zeros(DCM.n,DCM.n,size(DCM.U.u,2));
  	DCM.c = zeros(DCM.n,size(DCM.U.u,2));
    for int = 1:length(DCM.Y.name)
        DCM.c(int,1) = ~isempty(strfind(DCM.Y.name{int},'LOcC')) | ~isempty(strfind(DCM.Y.name{int},'MVOcC')) | ~isempty(strfind(DCM.Y.name{int},'PoG')) | ~isempty(strfind(DCM.Y.name{int},'PrG'));
    end
end


% check whether the folder exists
if ( ~exist(fullfile(foldername, Subject, 'FirstLevel_DCM', ['tapping_' handedness_folder{handedness}], parcellations{parcellation_type}),'dir') )
    mkdir(fullfile(foldername, Subject, 'FirstLevel_DCM', ['tapping_' handedness_folder{handedness}], parcellations{parcellation_type}))
end


% detrend and scale the data
if ( options.scale )
    
    % detrend data
    DCM.Y.y = spm_detrend(DCM.Y.y);

    % scale data
    scale   = max(max((DCM.Y.y))) - min(min((DCM.Y.y)));
    scale   = 4/max(scale,4);
    DCM.Y.y = DCM.Y.y*scale;

end


% slightly shift the data to account for faster hemodynamic response
options.u_shift = -8;

% use conservative filter (beneficial for sparse rDCM; as observed in simulations)
options.filter_str = 3.5;

% empirical analysis
type = 'r';


% set the prior type of the endogenous matrix
DCM.options.wide_priors = 0;


% create the directory
if ( ~exist(fullfile(foldername, Subject, 'FirstLevel_DCM', ['tapping_' handedness_folder{handedness}], parcellations{parcellation_type}, ['sparse_regressionDCM' scale_name{scale+1} parall_name{parallelized+1}], input_name{restrictInputs+1}),'dir') )
    mkdir(fullfile(foldername, Subject, 'FirstLevel_DCM', ['tapping_' handedness_folder{handedness}], parcellations{parcellation_type}, ['sparse_regressionDCM' scale_name{scale+1} parall_name{parallelized+1}], input_name{restrictInputs+1}))
end


% display the progress
disp(['Evaluating subject ' num2str(subject) ' - model ' num2str(model)])
disp(' ')

% get time
currentTimer = tic;


% specify all the p0 values
p0_array = 0.4:0.05:0.95;


% specify the options for the rDCM analysis
options.p0_all          = p0_array(p0_ind);
options.iter            = 100;
options.restrictInputs  = restrictInputs;
options.p0_inform       = 0;


% specify the number of cores to use (for parallelized code)
if ( parallelized ), options.NrCores = 16; end


% store in intermediate file, if only single p0 is estimated
if ( length(p0_ind) == 1 )
    p0_txt = ['00' num2str(p0_ind)];
    p0_txt = ['_' p0_txt(end-2:end)];
else
    p0_txt = '';
end


% check whether file has already been estimated
if ( exist(fullfile(foldername, Subject, 'FirstLevel_DCM', ['tapping_' handedness_folder{handedness}], parcellations{parcellation_type}, ['sparse_regressionDCM' scale_name{scale+1} parall_name{parallelized+1}], input_name{restrictInputs+1}, [filename '_rDCM' p0_txt '.mat']),'file') )
    pause(600)
    return
end


% run the rDCM analysis
[output, options] = tapas_rdcm_estimate(DCM, type, options, 2);

% output elapsed time
time_rDCM = toc(currentTimer);
disp(['Elapsed time is ' num2str(time_rDCM) ' seconds.'])


% store the estimation time
output.time.time_rDCM = time_rDCM;


% save the estimated results
if ( ~isempty(output) )
    save(fullfile(foldername, Subject, 'FirstLevel_DCM', ['tapping_' handedness_folder{handedness}], parcellations{parcellation_type}, ['sparse_regressionDCM' scale_name{scale+1} parall_name{parallelized+1}], input_name{restrictInputs+1}, [filename '_rDCM' p0_txt '.mat']),'output')
end

% save the estimated results
if ( ~isempty(options) )
    save(fullfile(foldername, Subject, 'FirstLevel_DCM', ['tapping_' handedness_folder{handedness}], parcellations{parcellation_type}, ['sparse_regressionDCM' scale_name{scale+1} parall_name{parallelized+1}], input_name{restrictInputs+1}, [filename '_rDCM' p0_txt '_options.mat']),'options')
end

end
