function summarize_srDCM_parameter_estimates_MainExperiment(subjects_analyze,model,aspirin,handedness,restrictInputs,parcellation_type,delete_files)
% Colelct the results for the different p0 values for each subject and 
% model from the whole-brain Dynamic Causal Models (DCMs) for the hand 
% movement dataset. The function then summarizes the different reults and 
% deletes the individual p0 files for the sake of memory (if specified as 
% an input argument). Parameter estimates have been computed using 
% regression DCM (rDCM) under sparsity constraints.
% 
% Input:
%   subjects_analyze    -- subjects to include
%   model               -- model to analyze
%   aspirin             -- (0) no aspirin and (1) aspirin
% 	handedness         	-- left- or right-hand movement dataset
%   restrictInputs      -- fix inputs to the "correct" regions
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainetome 2016, (4) Glasser 2016 & Buckner 2011
%   delete_files        -- delete p0 values once summary file is created
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end

% define the experiment folders
aspirin_folder    = {'wo_Aspirin/','Aspirin/'};
handedness_folder = {'left','right'};

% add the relevant paths and set the foldername
addpath(FilenameInfo.SPM_path)
data_foldername  = FilenameInfo.DataPath;

% define the different names
scale_name      = {'_noScale',''};
input_name      = {'allInput','restrictInputs'};

% set the parcellation
parcellations     = {'Glasser2016','AAL','Brainnetome2016','Glasser2016_Buckner2011'};

% set the foldername
foldername = [data_foldername aspirin_folder{aspirin+1}];

% set no scaling
scale = 0;


% get the subject list
Subject_List = dir(fullfile(foldername,'1*'));


% define the different p0
p0_all = 1:12;

% array containing missing DCMs
missing_DCMs = zeros(length(Subject_List),length(p0_all));


% iterate over all subjects
for subject = subjects_analyze

    % subject name
    Subject = Subject_List(subject).name;
    
    % set the directory
    foldername_rDCM = fullfile(foldername, Subject, 'FirstLevel_DCM', ['tapping_' handedness_folder{handedness}], parcellations{parcellation_type}, ['sparse_regressionDCM' scale_name{scale+1}], input_name{restrictInputs+1});
    
    % display progress
    disp(['Summarize results subject: ' Subject ' - model: ' num2str(model)])
    
    % devine results array
    temp_output	= cell(1,length(p0_all));
    temp_F      = NaN(1,length(p0_all));
    counter     = 0;

    for p0_int = 1:length(p0_all)
        
        % file name
        p0_text = ['00' num2str(p0_all(p0_int))];
        p0_text = p0_text(end-2:end);
        
        % define the filename
        filename = ['DCM_hand_model' num2str(model) '_rDCM_' p0_text '.mat'];
        
        % specify the complete path
        filename_p0 = dir([foldername_rDCM '/' filename]);
        
        % load the file
        if ( ~isempty(filename_p0) )

            % increase counter
            counter = counter + 1;

            % load the data
            temp = load([foldername_rDCM '/' filename_p0(1).name]);

            % asing output
            temp_output{p0_int}	= temp.output;
            temp_F(p0_int)      = temp.output.logF;
            
        else
            
            % store missing DCMs
            missing_DCMs(subject,p0_int) = 1;
            
        end

        % clear the p0 filename
        clear filename_p0

    end

    % clear the temp structure
    clear temp

    % get the best (max_F) solution and save the combined file
    if ( counter == length(p0_all) )

        % get the best
        [~,F_max_int] = max(temp_F);
        output        = temp_output{F_max_int};

        % save the results
        if ( ~isempty(output) )
            save(fullfile(foldername_rDCM, ['DCM_hand_model' num2str(model) '_rDCM.mat']),'output')
        end
        
        % file name
        p0_text = ['00' num2str(F_max_int)];
        p0_text = p0_text(end-2:end);
        
        % save the corresponding options file
        temp_opt = load(fullfile(foldername_rDCM, ['DCM_hand_model' num2str(model) '_rDCM_' p0_text '_options.mat']));
        options  = temp_opt.options;
        
        % save the results
        if ( ~isempty(options) )
            save(fullfile(foldername_rDCM, ['DCM_hand_model' num2str(model) '_rDCM_options.mat']),'options')
        end
        
        % delete the temporary files
        if ( delete_files )
            for p0_int = p0_all

                % get the filename
                p0_text = ['00' num2str(p0_int)];
                p0_text = p0_text(end-2:end);
                
                % set the filename
                filename_p0 = dir(fullfile(foldername_rDCM, ['DCM_hand_model' num2str(model) '_rDCM_' p0_text '.mat']));
                
                % delete the p0 value
                delete(fullfile(foldername_rDCM, filename_p0(1).name))
                
                % set the filename
                filename_options_p0 = dir(fullfile(foldername_rDCM, ['DCM_hand_model' num2str(model) '_rDCM_' p0_text '_options.mat']));
                
                % delete the p0 value
                delete(fullfile(foldername_rDCM, filename_options_p0(1).name))
                
            end
        end
    end
    
    % clear the output variable
    clear output options
    
end


% display which DCMs are missing in which subjects
[sub_miss,p0_miss] = find(missing_DCMs==1);
if ( ~isempty(sub_miss) )
    for int = 1:length(sub_miss)
        disp(['Aspirin: ' num2str(aspirin) ' - Handedness: ' num2str(handedness) ' - Subject: ' num2str(sub_miss(int)) ' - p0_ind: ' num2str(p0_all(p0_miss(int)))])
    end
else
    disp('All DCMs estimated')
end

end