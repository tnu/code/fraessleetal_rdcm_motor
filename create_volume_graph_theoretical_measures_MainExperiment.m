function create_volume_graph_theoretical_measures_MainExperiment(model,handedness,rdcm_variant,parcellation_type,restrictInputs)
% Generate volumes of the graph theoretical measures from the whole-brain 
% Dynamic Causal Models (DCMs) for the hand movement dataset. Parameter 
% estimates have been computed using regression DCM (rDCM).
% 
% This function reads the results from the graph theoretical analysis and
% asigns them to the respective parcel. This is then written as a nifti
% image. The nifti image can then be displayed using SurfIce (or other
% illustration tools like the Human Connectome Workbench).
% 
% Input:
%   model               -- model to analyze
%   handedness          -- (1) left hand, (2) right hand
%   rdcm_variant        -- (1) anatomically informed rDCM, (2) sparse rDCM
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainetome 2016, (4) Glasser 2016 & Buckner 2011
%   restrictInputs      -- inputs: (0) all inputs, (1) restrict inputs
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% add SPM
addpath(FilenameInfo.SPM_path);


% define the experiment folders
handedness_folder   = {'left','right'};
scale_name          = {'_noScale',''};
input_name          = {'allInput','restrictInputs'};

% set the parcellation
parcellations = {'Glasser2016','AAL','Brainnetome2016','Glasser2016_Buckner2011'};

% mask foldername
parcellation_folder = fullfile(FilenameInfo.LongTermStorage_path,'parcellation',parcellations{parcellation_type});
mask_folder         = fullfile(parcellation_folder,'BNA_PM_3D_246');

% foldername
foldername  = fullfile(FilenameInfo.DataPath,'all_Aspirin');

% no scaling of fMRI data
scale = 0;

% get all the mask files
all_regions = dir(fullfile(mask_folder, '*.nii'));

% load the missing regions
load(fullfile(FilenameInfo.DataPath,'VoI_coordinates','IBS',parcellations{parcellation_type},'MissingROIs.mat'));

Nr_regions = 1:246;
Nr_regions = setdiff(Nr_regions,allMissing_regions_ind);
ind_region = find(Nr_regions == 57);


% load the mean graph-theoretical results
if ( rdcm_variant == 1 )
    temp    = load(fullfile(foldername,'DCM_GroupAnalysis',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome'],['rDCM_GraphTheory_model' num2str(model) '_' handedness_folder{handedness} 'Hand.mat']));
elseif ( rdcm_variant == 2 )
    temp    = load(fullfile(foldername,'DCM_GroupAnalysis',parcellations{parcellation_type},['sparse_regressionDCM' scale_name{scale+1}],input_name{restrictInputs+1},['rDCM_GraphTheory_model' num2str(model) '_' handedness_folder{handedness} 'Hand.mat']));
end


% results structure
results = temp.results;


% initialize the arrays
id_all      = NaN(length(results),length(results(1).degree.id));
od_all      = NaN(length(results),length(results(1).degree.id));
deg_all     = NaN(length(results),length(results(1).degree.id));
is_all      = NaN(length(results),length(results(1).degree.id));
os_all      = NaN(length(results),length(results(1).degree.id));
str_all     = NaN(length(results),length(results(1).degree.id));
ccoef_all   = NaN(length(results),length(results(1).degree.id));
E_local_all	= NaN(length(results),length(results(1).degree.id));
BC_all      = NaN(length(results),length(results(1).degree.id));
core_all   	= NaN(length(results),length(results(1).degree.id));
M_all       = NaN(length(results),length(results(1).degree.id));
trans_all   = NaN(length(results),1);
kden_all    = NaN(length(results),1);
Q_all       = NaN(length(results),1);


% get all the graph-theoretical measures
for subject = 1:length(results)
    if ( isfield(results(subject),'degree') )
    
        % degree
        id_all(subject,:)   = results(subject).degree.id;
        od_all(subject,:)   = results(subject).degree.od;
        deg_all(subject,:)  = results(subject).degree.deg;

        % strengths
        is_all(subject,:)   = results(subject).degree.is;
        os_all(subject,:)   = results(subject).degree.os;
        str_all(subject,:)  = results(subject).degree.str;

        % density
        kden_all(subject)       = results(subject).density.kden;

        % clustering
        ccoef_all(subject,:)    = results(subject).cluster.ccoef';
        E_local_all(subject,:)	= results(subject).cluster.E_local';
        trans_all(subject)      = results(subject).cluster.trans;

        % betweenness centrality
        BC_all(subject,:)       = results(subject).centrality.BC';

        % community
        M_all(subject,:)        = results(subject).community.M';
        Q_all(subject)          = results(subject).community.Q;

        % stucture
        core_all(subject,:)     = results(subject).structure.core';
    
    end
end


% evaluate the average over subjects
mean_BC_all = nanmean(BC_all);
mean_str_all = nanmean(str_all);


% create novel folder
if ( rdcm_variant == 1 )
    graph_folder = fullfile(foldername,'DCM_GroupAnalysis',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome'],'GraphTheory');
else
    graph_folder = fullfile(foldername,'DCM_GroupAnalysis',parcellations{parcellation_type},['sparse_regressionDCM' scale_name{scale+1}],input_name{restrictInputs+1},'GraphTheory');
end

% create new folder for results
if ( ~exist(graph_folder,'dir') )
    mkdir(graph_folder)
end


% delete previous files
warning off
delete(fullfile(graph_folder,['BetweenCentrality_WholeBrain_model' num2str(model) '_' handedness_folder{handedness} 'Hand.nii']))
delete(fullfile(graph_folder,['NodeStrengthInOut_WholeBrain_model' num2str(model) '_' handedness_folder{handedness} 'Hand.nii']))
delete(fullfile(graph_folder,['DiffBetweenCentrality_WholeBrain_model' num2str(model) '_' handedness_folder{handedness} 'Hand.nii']))
delete(fullfile(graph_folder,['DiffNodeStrengthInOut_WholeBrain_model' num2str(model) '_' handedness_folder{handedness} 'Hand.nii']))
warning on

% iterate over graph theoretical measures and regions
for graph_measure = 1:2
    
    % calculate the volumes of the graph theoretical results
    for region = [ind_region:length(Nr_regions) 1:ind_region-1]

        % input image
        Vi = fullfile(mask_folder,all_regions(Nr_regions(region)).name);

        % output image
        Vo = fullfile(graph_folder,[handedness_folder{handedness}(1) all_regions(Nr_regions(region)).name]);

        % set the flags
        flags.dmtx   = 0;
        flags.mask   = 0;
        flags.interp = 1;
        flags.dtype  = 4;


        % specify the graph-theoretical measure to analyse
        if ( graph_measure == 1 )
            mean_gtm = mean_BC_all(region);
            gtm_name = ['BetweenCentrality_WholeBrain_model' num2str(model) '_' handedness_folder{handedness} 'Hand'];
        elseif ( graph_measure == 2 )
            mean_gtm = mean_str_all(region);
            gtm_name = ['NodeStrengthInOut_WholeBrain_model' num2str(model) '_' handedness_folder{handedness} 'Hand'];
        end


        % specify the expression
        f = [num2str(mean_gtm) ' .* (i1>33)'];

        % write the image
        Vo_new = spm_imcalc(Vi,Vo,f,flags);


        % create overall output image
        if ( region ~= ind_region )

            % specify input and output image
            Vi = [cellstr(fullfile(graph_folder,[gtm_name '.nii'])); cellstr(Vo)];
            Vo = fullfile(graph_folder,[gtm_name '.nii']);

            % specify the expression
            f = 'i1 + (i2 .* (i1 == 0))';

            % write the image
            Vo_new = spm_imcalc(Vi,Vo,f,flags);

        elseif ( region == ind_region )

            % input image
            Vi = fullfile(graph_folder,[handedness_folder{handedness}(1) all_regions(Nr_regions(region)).name]);

            % output image
            Vo = fullfile(graph_folder,[gtm_name '.nii']);

            % set the flags
            flags.dmtx   = 0;
            flags.mask   = 0;
            flags.interp = 1;
            flags.dtype  = 4;

            % specify the expression
            f = 'i1';    

            % write the image
            Vo_new = spm_imcalc(Vi,Vo,f,flags);

        end
    end

    % specify input and output image
    Vi = [cellstr(fullfile(parcellation_folder,'template.nii')); cellstr(fullfile(graph_folder,[gtm_name '.nii']))];
    Vo = fullfile(graph_folder,[gtm_name '.nii']);

    % specify the expression
    f = 'i1 + i2 - i1';

    % write the image to reslice to a lower resolution (speeds up surfrend)
    Vo_new = spm_imcalc(Vi,Vo,f,flags);



    % write a difference image (left vs right hemisphere)
    for region = [ind_region:length(Nr_regions) 1:ind_region-1]

        % need left and right mask
        if ( mod(region,2) == 0 )

            % specify the difference graph-theoretical measure to analyse
            if ( graph_measure == 1 )
                diff_mean_gtm = mean_BC_all(region)-mean_BC_all(region-1);
                gtm_name = ['DiffBetweenCentrality_WholeBrain_model' num2str(model) '_' handedness_folder{handedness} 'Hand'];
            elseif ( graph_measure == 2 )
                diff_mean_gtm = mean_str_all(region)-mean_str_all(region-1);
                gtm_name = ['DiffNodeStrengthInOut_WholeBrain_model' num2str(model) '_' handedness_folder{handedness} 'Hand'];
            end

            % write difference in both masks
            for region_pair = region-1:region

                % input image
                Vi = fullfile(mask_folder,all_regions(Nr_regions(region_pair)).name);

                % output image
                Vo = fullfile(graph_folder,[handedness_folder{handedness}(1) all_regions(Nr_regions(region)).name]);

                % set the flags
                flags.dmtx   = 0;
                flags.mask   = 0;
                flags.interp = 1;
                flags.dtype  = 4;

                % specify the expression
                f = [num2str(diff_mean_gtm) ' .* (i1>33)'];

                % write the image
                Vo_new = spm_imcalc(Vi,Vo,f,flags);


                % create overall output image
                if ( region_pair ~= ind_region )

                    % specify input and output image
                    Vi = [cellstr(fullfile(graph_folder,[gtm_name '.nii'])); cellstr(Vo)];
                    Vo = fullfile(graph_folder,[gtm_name '.nii']);

                    % specify the expression
                    f = 'i1 + (i2 .* (i1 == 0))';

                    % write the image
                    Vo_new = spm_imcalc(Vi,Vo,f,flags);

                elseif ( region_pair == ind_region )

                    % input image
                    Vi = fullfile(graph_folder,[handedness_folder{handedness}(1) all_regions(Nr_regions(region)).name]);

                    % output image
                    Vo = fullfile(graph_folder,[gtm_name '.nii']);

                    % set the flags
                    flags.dmtx   = 0;
                    flags.mask   = 0;
                    flags.interp = 1;
                    flags.dtype  = 4;

                    % specify the expression
                    f = 'i1';    

                    % write the image
                    Vo_new = spm_imcalc(Vi,Vo,f,flags);

                end
            end
        end
    end

    % specify input and output image
    Vi = [cellstr(fullfile(parcellation_folder,'template.nii')); cellstr(fullfile(graph_folder,[gtm_name '.nii']))];
    Vo = fullfile(graph_folder,[gtm_name '.nii']);

    % specify the expression
    f = 'i1 + i2 - i1';

    % write the image
    Vo_new = spm_imcalc(Vi,Vo,f,flags);
    
end

% delete temporary files
delete(fullfile(graph_folder,[handedness_folder{handedness}(1) '*nii']))

end
