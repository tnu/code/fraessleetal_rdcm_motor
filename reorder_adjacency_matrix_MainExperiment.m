function matrix_reorder = reorder_adjacency_matrix_MainExperiment(matrix)
% This helper function takes an adjacency matrix from the Human Brainnetome 
% atlas (ordered region1_LH, region1_RH, region2_LH, region2_RH, ...) and 
% separates the left- and right-hemispheric regions by reordering columns
% and rows accordingly.
%
% Input:
%   matrix              -- adjacency matrix
%   
% Output:
%   matrix_reorder      -- adjacency matrix reordered (LH, RH)
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% endogenous connectivity matrix or driving inputs
if ( size(matrix,1) == size(matrix,2) )

    % get individual parts
    LU = matrix(1:2:size(matrix,1),1:2:size(matrix,2));
    RU = matrix(1:2:size(matrix,1),2:2:size(matrix,2));
    LB = matrix(2:2:size(matrix,1),1:2:size(matrix,2));
    RB = matrix(2:2:size(matrix,1),2:2:size(matrix,2));

    % combine the parts
    matrix_reorder = [[LU RU];[LB RB]];
    
else
    
    % reorder matrix
    matrix_reorder = matrix([1:2:size(matrix,1),2:2:size(matrix,1)],:);
    
end

end