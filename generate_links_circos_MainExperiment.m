function generate_links_circos_MainExperiment(adjacency_matrix,parcellation_type,filename,circos_opt)
% Takes an adjacency matrix and creates the link file that is needed by
% Circos to create a connectogram. The filename of the link_file also has
% to be provided to the function.
% 
% Input:
%   adjacency_matrix    -- adjacency matrix
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL
%   filename            -- filename for storing the Circos link file
%   circos_opt          -- different options that specify the Circos plot
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% set the parcellation
parcellations = {'Glasser2016','AAL','Brainnetome2016','Glasser2016_Buckner2011'};


% check for transparency options
if ( ~isfield(circos_opt,'trans_con') )
    circos_opt.trans_con = zeros(size(adjacency_matrix));
end


% represent strength of connection 
if ( strcmp(circos_opt.normalize,'relative') )
    adjacency_matrix_normalized = abs(adjacency_matrix);
    binary_mask = adjacency_matrix_normalized ~= 0;
    x(1)        = min(adjacency_matrix_normalized(adjacency_matrix_normalized~=0));
    x(2)        = max(adjacency_matrix_normalized(adjacency_matrix_normalized~=0));
    P           = polyfit(x,[1 circos_opt.scaling],1);
    adjacency_matrix_normalized = P(1) * adjacency_matrix_normalized + P(2);
    adjacency_matrix_normalized = zeros(size(adjacency_matrix_normalized)) + binary_mask .* adjacency_matrix_normalized;
elseif ( strcmp(circos_opt.normalize,'absolute') )
    adjacency_matrix_normalized = abs(adjacency_matrix);
    min_value                   = min(adjacency_matrix_normalized(adjacency_matrix_normalized~=0));
    adjacency_matrix_normalized = adjacency_matrix_normalized ./ min_value;
elseif ( strcmp(circos_opt.normalize,'none') )
    binary_mask                 = adjacency_matrix ~= 0;
    adjacency_matrix_normalized = zeros(size(adjacency_matrix)) + binary_mask .* circos_opt.scaling;
end


% select the Circos folder for the respective parcellation
foldername = fullfile(FilenameInfo.LongTermStorage_path,'parcellation',parcellations{parcellation_type},'circos_atlas-master');

% get the names of the brain regions and the start/end
if ( parcellation_type == 3 )
    [lobe,~,~,st,en,~]  = textread(fullfile(foldername,'subregion_band_name_color.txt'),'%s%d%s%d%d%s');
end

% open the textfile to store the results
fid = fopen(filename,'w');

% write the links with red/green color (red = inhibitory, green = exitatory) 
for n = 1:size(adjacency_matrix,1)
    for i = 1:size(adjacency_matrix,2)
        if adjacency_matrix(n,i) ~= 0
            if ( adjacency_matrix(n,i) < 0 )
                col = circos_opt.col{circos_opt.trans_con(n,i)+1,1};
            elseif ( adjacency_matrix(n,i) > 0 )
                col = circos_opt.col{circos_opt.trans_con(n,i)+1,2};
            end
            
            fprintf(fid,'%s %d %d %s %d %d val=%f,col=%s\n',lobe{n},st(n),en(n),lobe{i},st(i),en(i),abs(adjacency_matrix_normalized(n,i)),col);
        end
    end
end

% close the file
fclose(fid);

% display the next step for Cicros analysis
disp(' ')
disp('For CIRCOS Analysis: Change path to link file:')
disp(filename)

end