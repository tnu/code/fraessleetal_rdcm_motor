function first_level_MainExperiment(subject,handedness,aspirin)
% Specifies the first-level GLM and then estimates the model. Additionally
% it creates the contrast that is later used to extract the time series and
% adjust for effects of no interest. Notably, the function uses
% preprocessed data from a previous study.
%
% Input:
%   subject             -- subject to be analyzed
% 	handedness         	-- left- or right-hand movement dataset
%   aspirin             -- subjects (0) without or (1) with aspirin
% 
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2018
% Copyright 2018 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% turn off warnings
warning off

% define important folders
aspirin_folder = {'wo_Aspirin/','Aspirin/'};
handedness_folder = {'left','right'};

% set the default
if ( isempty(handedness) )
    handedness_analyze = [1 2];
else
    handedness_analyze = handedness;
end

% set the default
if ( isempty(aspirin) )
    aspirin_analyze = [0 1];
else
    aspirin_analyze = aspirin;
end


% iterate over different conditions
for aspirin = aspirin_analyze

    % set the paths
    addpath(genpath(FilenameInfo.SPM_path));
    
    % set folder names
    folder_experiment = fullfile(FilenameInfo.LongTermStorage_path,aspirin_folder{aspirin+1});
    folder_results    = fullfile(FilenameInfo.DataPath,aspirin_folder{aspirin+1});
    
    
    % initialize spm
    spm('Defaults','fMRI');
    spm_jobman('initcfg');
    

    % subject list
    subject_list = dir(fullfile(folder_experiment, '1*'));

    % specify the subject
    Subject = subject_list(subject).name;
    disp(['Subject: ' Subject])
    
    
    % iterate over different hand conditions
    for handedness = handedness_analyze

        % load the regressors for the GLM
        regressor_folder = dir(fullfile(folder_experiment, Subject, 'regressoren', ['fs_' handedness_folder{handedness}(1)], 'Session*'));
        factors = load(fullfile(folder_experiment, Subject, 'regressoren', ['fs_' handedness_folder{handedness}(1)], regressor_folder(1).name));

        % specify the SPM results folder
        SPM_foldername = fullfile(folder_results, Subject, 'FirstLevel', 'IBS', ['tapping_' handedness_folder{handedness}]);

        % create SPM directory
        if ( ~exist(SPM_foldername,'dir') )
            mkdir(SPM_foldername)
        end

        % get all the functional images
        functional_folder = dir(fullfile(folder_experiment, Subject, 'functional', 'fs*'));
        f = spm_select('FPList', fullfile(folder_experiment, Subject, 'functional', functional_folder(handedness).name), '^sw.*\.img$');
        
        % get the confound parameters
        confound_file = dir(fullfile(folder_experiment, Subject, 'regressoren', ['fs_' handedness_folder{handedness}(1)], 'combconfound.mat'));

        % clear the matlabbatch
        clear matlabbatch

        % model specification
        matlabbatch{1}.spm.stats.fmri_spec.dir                   = cellstr(SPM_foldername);
        matlabbatch{1}.spm.stats.fmri_spec.timing.units          = 'secs';
        matlabbatch{1}.spm.stats.fmri_spec.timing.RT             = 2;
        matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t         = 16;
        matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0        = 8;
        
        matlabbatch{1}.spm.stats.fmri_spec.sess.scans            = cellstr(f);
        matlabbatch{1}.spm.stats.fmri_spec.sess.cond(1).name     = [handedness_folder{handedness} ' hand'];
        matlabbatch{1}.spm.stats.fmri_spec.sess.cond(1).onset    = factors.onsets{1};
        matlabbatch{1}.spm.stats.fmri_spec.sess.cond(1).duration = 0;
        matlabbatch{1}.spm.stats.fmri_spec.sess.multi_reg        = cellstr(fullfile(folder_experiment, Subject, 'regressoren', ['fs_' handedness_folder{handedness}(1)], confound_file(1).name));
        matlabbatch{1}.spm.stats.fmri_spec.sess.hpf              = 128;
        matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs      = [1 1];
        
        
        % model estimation
        matlabbatch{2}.spm.stats.fmri_est.spmmat = cellstr(fullfile(SPM_foldername, 'SPM.mat'));
        
        
        % model inference
        matlabbatch{3}.spm.stats.con.spmmat                  = cellstr(fullfile(SPM_foldername, 'SPM.mat'));
        matlabbatch{3}.spm.stats.con.consess{1}.tcon.name    = [handedness_folder{handedness} ' hand'];
        matlabbatch{3}.spm.stats.con.consess{1}.tcon.weights = [1 0 0];
        matlabbatch{3}.spm.stats.con.consess{2}.fcon.name    = 'EoI';
        matlabbatch{3}.spm.stats.con.consess{2}.fcon.weights = eye(1);

        % remove the existing SPM file
        if ( exist(fullfile(SPM_foldername, 'SPM.mat'),'file') )
            delete(fullfile(SPM_foldername, 'SPM.mat'))
        end
        
        % evaluate the SPM.mat
        spm_jobman('run',matlabbatch);
        
    end
end

end