function compare_LR_FC_MainExperiment(aspirin,fc_mode,parcellation_type,p_corr,onCluster)
% Compare parameter estimates from a functional connectivity analysis for 
% left- and right-hand movement for the hand movement dataset. Functional
% connectivity measures have been computed using the BrainConnectivity
% toolbox.
% 
% This function reads the summary file containing all the functional 
% connectivity estimates. The function then evaluates the difference 
% between left and right hand.
% 
% Input:
%   aspirin             -- (0) no aspirin and (1) aspirin
%   fc_mode             -- (1) Pearson correlation, (2) Partial correlation
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainnetome 2016, (4) Glasser 2016 & Buckner 2011
%   p_corr              -- mutliple comparison correction: (1) FDR, (0) uncorrected
%   onCluster           -- operating system: (1) Euler, (0) local machine
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the experiment folders
aspirin_folder    = {'wo_Aspirin/','Aspirin/','all_Aspirin/'};

% add the relevant paths and set the foldername
exp_foldername  = FilenameInfo.DataPath;
addpath(fullfile(FilenameInfo.MATLAB_path,'fdr_bh'))
addpath(fullfile(FilenameInfo.MATLAB_path,'bipolar_colormap'))
addpath(fullfile(FilenameInfo.MATLAB_path,'circularGraph'))


% define the different names
pcorr_name      = {'_uncorrected','','_top'};
fc_name         = {'CrossCorr','PartialCorr'};

% set the parcellation
parcellations = {'Glasser2016','AAL','Brainnetome2016','Glasser2016_Buckner2011'};


% close all figures
close all

% display progress
disp('Loading data...')


if ( ~isempty(aspirin) )
    
    % set the SPM path and the path to the experiment
    foldername = [exp_foldername aspirin_folder{aspirin+1}];
    
    % load the functional connectivity files
    temp = load(fullfile(foldername,'FC_GroupAnalysis','tapping_left',parcellations{parcellation_type},'FC_measures.mat'));
    
    % get the respective functional connectivity measure
    if ( fc_mode == 1 )
        FC_data_AllSubjects = temp.FC_measures.fullCorr_AllSubjects;
    elseif ( fc_mode == 2 )
        FC_data_AllSubjects = temp.FC_measures.partialCorr_AllSubjects;
    end
    
    % asign the functional connectivity results
    FC_allSubjects_left = FC_data_AllSubjects;
    
    % load the functional connectivity files
    temp = load(fullfile(foldername,'FC_GroupAnalysis','tapping_right',parcellations{parcellation_type},'FC_measures.mat'));
    
    % get the respective functional connectivity measure
    if ( fc_mode == 1 )
        FC_data_AllSubjects = temp.FC_measures.fullCorr_AllSubjects;
    elseif ( fc_mode == 2 )
        FC_data_AllSubjects = temp.FC_measures.partialCorr_AllSubjects;
    end
    
    % asign the functional connectivity results
    FC_allSubjects_right = FC_data_AllSubjects;
    
else
    
    % iterate over both conditions
    for aspirin = [0 1]
        
        % set the SPM path and the path to the experiment
        foldername = [exp_foldername aspirin_folder{aspirin+1}];

        % load the functional connectivity files for left hand
        temp = load(fullfile(foldername,'FC_GroupAnalysis','tapping_left',parcellations{parcellation_type},'FC_measures.mat'));
        
        % get the respective functional connectivity measure
        if ( fc_mode == 1 )
            FC_data_AllSubjects = temp.FC_measures.fullCorr_AllSubjects;
        elseif ( fc_mode == 2 )
            FC_data_AllSubjects = temp.FC_measures.partialCorr_AllSubjects;
        end
        
        % asign the functional connectivity estimates for left hand
        if ( aspirin == 0 )
            FC_allSubjects_left = FC_data_AllSubjects;
        else
            for int = 1:size(temp.FC_measures.fullCorr_AllSubjects,1)
                for int2 = 1:size(temp.FC_measures.fullCorr_AllSubjects,2)
                    FC_allSubjects_left{int,int2}  = [FC_allSubjects_left{int,int2}; FC_data_AllSubjects{int,int2}];
                end
            end
        end
        
        % load the functional connectivity files for left hand
        temp = load(fullfile(foldername,'FC_GroupAnalysis','tapping_right',parcellations{parcellation_type},'FC_measures.mat'));
        
        % get the respective functional connectivity measure
        if ( fc_mode == 1 )
            FC_data_AllSubjects = temp.FC_measures.fullCorr_AllSubjects;
        elseif ( fc_mode == 2 )
            FC_data_AllSubjects = temp.FC_measures.partialCorr_AllSubjects;
        end
        
        % asign the functional connectivity estimates for left hand
        if ( aspirin == 0 )
            FC_allSubjects_right = FC_data_AllSubjects;
        else
            for int = 1:size(temp.FC_measures.fullCorr_AllSubjects,1)
                for int2 = 1:size(temp.FC_measures.fullCorr_AllSubjects,2)
                    FC_allSubjects_right{int,int2}  = [FC_allSubjects_right{int,int2}; FC_data_AllSubjects{int,int2}];
                end
            end
        end  
    end
    
    % set aspirin condition to all subjects
    aspirin     = 2;
    
end


% specify a indicator vector
if ( aspirin == 0 )
    vector = [1:7 10:size(FC_allSubjects_left{1,1},1)];
elseif ( aspirin == 1 )
    vector = [1:4 7:size(FC_allSubjects_left{1,1},1)];
elseif ( aspirin == 2 )
    vector = [[1:7 10:14] [1:4 7:15]+14];
end


% dsiplay progress
disp(['# subjects found: ' num2str(length(vector))])
disp('Computing left-right difference...')

% results arrays
mean_FC_allSubjects_LR = NaN(size(FC_allSubjects_left));
pVal_FC_allSubjects_LR = NaN(size(FC_allSubjects_left));
TVal_FC_allSubjects_LR = NaN(size(FC_allSubjects_left));


% get summaries
for int = 1:size(FC_allSubjects_left,1)
    for int2 = 1:size(FC_allSubjects_left,2)

        % compute the difference between left- and right-hand movements
        mean_FC_allSubjects_LR(int,int2) = nanmean(FC_allSubjects_left{int,int2}(vector)-FC_allSubjects_right{int,int2}(vector));

        % check for a significant difference
        [~,P,~,STATS] = ttest(atanh(FC_allSubjects_left{int,int2}(vector)),atanh(FC_allSubjects_right{int,int2}(vector)));
        pVal_FC_allSubjects_LR(int,int2) = P;
        
        % store the T statistics
        if ( isfinite(STATS.tstat) )
            TVal_FC_allSubjects_LR(int,int2) = STATS.tstat;
        else
            TVal_FC_allSubjects_LR(int,int2) = 0;
        end
    end
end


% compute the top connections (with maximal T value)
top_FC_allSubjects_LR = compute_top_connections_MainExperiment(TVal_FC_allSubjects_LR,[],500);


% set the threshold
if ( p_corr )
    p_all                    = pVal_FC_allSubjects_LR(:);
    p_all                    = p_all(isfinite(p_all));
    [~, crit_p, ~, ~]        = fdr_bh(p_all,0.05,'dep','no');
    threshold                = crit_p;
    disp(['Estimated p-Value (FDR-corrected): ' num2str(threshold)])
else
    threshold = 0.001;
    disp(['Estimated p-Value (uncorrected): ' num2str(threshold)])
end


% specify the folder where results should be stored
results_folder = fullfile(exp_foldername,'Figures','IBS',parcellations{parcellation_type},'FC_noScale','EffectOfHand',aspirin_folder{aspirin+1});

% create the directory
if ( ~exist(results_folder,'dir') ) 
    mkdir(results_folder)
else
    if ( exist(fullfile(results_folder,[fc_name{fc_mode} '_Difference_LR.ps']),'file') )
        delete(fullfile(results_folder,[fc_name{fc_mode} '_Difference_LR.ps']))
    end
end


% open the figures
figure('units','normalized','outerposition',[0 0 1 1])
figure('units','normalized','outerposition',[0 0 1 1])
figure('units','normalized','outerposition',[0 0 1 1])


% display the difference between left- and right-hand movements
figure(1)
imagesc(mean_FC_allSubjects_LR-diag(diag(mean_FC_allSubjects_LR)))
colormap('bipolar')
caxis([-0.5 0.5])
axis square
colorbar
title('mean adjacency matrix (FC) - effect of hand','FontSize',12)
xlabel('region (from)','FontSize',11)
ylabel('region (to)','FontSize',11)
set(gca,'xtick',[1 size(mean_FC_allSubjects_LR,1)/2 size(mean_FC_allSubjects_LR,1)])
set(gca,'ytick',[1 size(mean_FC_allSubjects_LR,1)/2 size(mean_FC_allSubjects_LR,1)])
print(gcf, '-dpsc', fullfile(results_folder,[fc_name{fc_mode} '_Difference_LR.ps']), '-append')

% display the significant difference between left- and right-hand movements
figure(2)
imagesc(pVal_FC_allSubjects_LR < threshold)
colormap('gray')
caxis([0 1])
axis square
colorbar
title('mean adjacency matrix (FC) - (sig) effect of hand','FontSize',12)
xlabel('region (from)','FontSize',11)
ylabel('region (to)','FontSize',11)
set(gca,'xtick',[1 size(mean_FC_allSubjects_LR,1)/2 size(mean_FC_allSubjects_LR,1)])
set(gca,'ytick',[1 size(mean_FC_allSubjects_LR,1)/2 size(mean_FC_allSubjects_LR,1)])
print(gcf, '-dpsc', fullfile(results_folder,[fc_name{fc_mode} '_Difference_LR.ps']), '-append')


% threshold the difference matrix
if ( p_corr ~= 0 )
    
    % get the thresholded difference matrix
    mean_FC_allSubjects_LR_display = mean_FC_allSubjects_LR .* (pVal_FC_allSubjects_LR <= threshold);
    
    % get the top connections (highest T values)
    if ( p_corr == 2 )
        mean_FC_allSubjects_LR_display_binary = mean_FC_allSubjects_LR_display == 0;
        mean_FC_allSubjects_LR_top            = top_FC_allSubjects_LR;
        mean_FC_allSubjects_LR_top            = mean_FC_allSubjects_LR_top - diag(diag(mean_FC_allSubjects_LR_top));
        mean_FC_allSubjects_LR_top            = mean_FC_allSubjects_LR_top + (top_FC_allSubjects_LR .* diag(diag(mean_FC_allSubjects_LR_display ~= 0)));
    end
else
    mean_FC_allSubjects_LR_display = mean_FC_allSubjects_LR .* (pVal_FC_allSubjects_LR < threshold);
end


% open figure
figure(3);

% create the connectogram
mean_FC_allSubjects_LR_store   = mean_FC_allSubjects_LR_display;
mean_FC_allSubjects_LR_display = abs(mean_FC_allSubjects_LR_display-diag(diag(mean_FC_allSubjects_LR_display)));
mean_FC_allSubjects_LR_display = [[mean_FC_allSubjects_LR_display(2:2:end,2:2:end),mean_FC_allSubjects_LR_display(2:2:end,1:2:end)];
                                        [mean_FC_allSubjects_LR_display(1:2:end,2:2:end),mean_FC_allSubjects_LR_display(1:2:end,1:2:end)]];

% store the connectogram
if ( ~isempty(mean_FC_allSubjects_LR_store) )
    
    % create folder
    if ( ~exist(fullfile(exp_foldername,aspirin_folder{aspirin+1},'FC_GroupAnalysis',parcellations{parcellation_type}),'dir') )
        mkdir(fullfile(exp_foldername,aspirin_folder{aspirin+1},'FC_GroupAnalysis',parcellations{parcellation_type}))
    end
    
    % store the connectogram
    save(fullfile(exp_foldername,aspirin_folder{aspirin+1},'FC_GroupAnalysis',parcellations{parcellation_type},[fc_name{fc_mode} '_Difference_LR' pcorr_name{p_corr+1} '.mat']),'mean_FC_allSubjects_LR_store')
end


% make directory for Circos
if ( ~exist(fullfile(results_folder,'Circos'),'dir') )
    mkdir(fullfile(results_folder,'Circos'))
end

% set the filename for the link files
filename_CIRCOS = fullfile(results_folder,'Circos',[fc_name{fc_mode} '_Difference_LR' pcorr_name{p_corr+1} '.txt']);

% option for circos file
circos_opt.scaling   = 16;
circos_opt.col       = {'255,0,0,1','44,162,95,1';'252,187,161,0.7','152,216,201,0.7'};
circos_opt.normalize = 'relative';
circos_opt.trans_con = mean_FC_allSubjects_LR_display_binary;

% create the Circos link file
generate_links_circos_MainExperiment(mean_FC_allSubjects_LR_top,parcellation_type,filename_CIRCOS,circos_opt)
 

% make directory for Circos
if ( ~exist(fullfile(results_folder,'BrainNet'),'dir') )
    mkdir(fullfile(results_folder,'BrainNet'))
end

% define the folder for the BrainNet files
folder_BrainNet   = fullfile(results_folder,'BrainNet/');
filename_BrainNet = [fc_name{fc_mode} '_Difference_LR' pcorr_name{p_corr+1}]; 

% create the BrainNet node and edge files
create_BrainNet_files_MainExperiment(mean_FC_allSubjects_LR_store,folder_BrainNet,filename_BrainNet)


% load the rDCM results for the region names
temp_RegionNames = load(fullfile(foldername,'DCM_GroupAnalysis',parcellations{parcellation_type},'regressionDCM_noScale_connectome', 'PosteriorParameterEstimates_model1.mat'));

                                         
% display the connectogram (using circularGraph.m)   
if ( ~onCluster )
    circularGraph(mean_FC_allSubjects_LR_display,'Label',strrep([temp_RegionNames.results.AllRegions(2:2:end), temp_RegionNames.results.AllRegions(1:2:end)],'_','\_'));
    print(gcf, '-dpsc', fullfile(results_folder,[fc_name{fc_mode} '_Difference_LR.ps']), '-append')
end

end