function get_rDCM_time_MainExperiment(aspirin,parcellation_type)
% Get the run times (computational burden) for the whole-brain Dynamic Causal 
% Models (DCMs) for the hand movement dataset. Parameter estimates have been 
% computed using regression DCM (rDCM).
% 
% This function reads the DCM files that have been estimated using
% regression DCM. The function stores the run times and also outputs those
% results in a table.
% 
% Input:
%   aspirin             -- (0) no aspirin and (1) aspirin
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL, (3) Brainnetome 2016, (4) Glasser 2016 & Buckner 2011
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the experiment folders
aspirin_folder    = {'wo_Aspirin/','Aspirin/','all_Aspirin/'};
handedness_folder = {'left','right'};

% add the relevant paths and set the foldername
pre_foldername = FilenameInfo.DataPath;


% scale fMRI data (default: 0)
scale = 0;


% check for aspirin condition
if ( isempty(aspirin) )
    aspirin_analyze = [0 1];
    aspirin_final   = 2; 
else
    aspirin_analyze = aspirin;
    aspirin_final   = aspirin;
end


% define cells
time_rDCM_VBinv_AllSubjects = cell(1,2);
time_rDCM_AllSubjects    	= cell(1,2);


% iterate over aspirin conditions
for aspirin = aspirin_analyze

    % define the different names
    foldername      = [pre_foldername aspirin_folder{aspirin+1}];
    scale_name      = {'_noScale',''};

    % set the parcellation
    parcellations = {'Glasser2016','AAL','Brainnetome2016','Glasser2016_Buckner2011'};
    
    % get the subject list
    Subject_List = dir(fullfile(foldername,'1*'));
    
    
    % specify a indicator vector
    if ( aspirin == 0 )
        vector = [1:7 10:length(Subject_List)];
    elseif ( aspirin == 1 )
        vector = [1:4 7:length(Subject_List)];
    end
    

    % iterate over all subjects
    for subject = 1:length(Subject_List)

        % subject name
        Subject = Subject_List(subject).name;

        % iterate over hand movements
        for handedness = 1:2

            % set the directory
            foldername_rDCM = fullfile(foldername, Subject, 'FirstLevel_DCM', ['tapping_' handedness_folder{handedness}], parcellations{parcellation_type}, ['regressionDCM' scale_name{scale+1} '_connectome']);
            
            % iterate over models
            for model = 1:3
            
                % define the filename
                filename = ['DCM_hand_model' num2str(model) '_rDCM.mat'];

                % get the data
                if ( exist(fullfile(foldername_rDCM,filename),'file') )

                    % display subject name
                    disp(['Subject: ' Subject ' - found'])

                    % load the DCM
                    temp           = load(fullfile(foldername_rDCM,filename));
                    output         = temp.output;

                else

                    % display subject name
                    if ( any(subject == vector) )
                        disp(['Subject: ' Subject ' - missing'])
                    else
                        disp(['Subject: ' Subject ' - not included'])
                    end

                    % define dummy times
                    output.time.time_rDCM_VBinv = NaN;
                    output.time.time_rDCM       = NaN;

                end

                % asign the negative free energy
                time_rDCM_VBinv_AllSubjects{model,handedness}	= [time_rDCM_VBinv_AllSubjects{1,handedness}; output.time.time_rDCM_VBinv];
                time_rDCM_AllSubjects{model,handedness}         = [time_rDCM_AllSubjects{1,handedness}; output.time.time_rDCM];
                
            end
        end
    end
end


% asign the results
results.time_rDCM_VBinv_AllSubjects = time_rDCM_VBinv_AllSubjects;
results.time_rDCM_AllSubjects    	= time_rDCM_AllSubjects;


% specify the folder where output logs should be stored
results_folder = fullfile(pre_foldername,'Logs',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome']);

% create the results folder
if ( ~exist(results_folder,'dir') )
    mkdir(results_folder)
end


% open a file
fileID = fopen(fullfile(results_folder, 'RunTime_rDCM.txt'),'w');

% write the run-times
fprintf(fileID,'Run-times (in seconds): rDCM under anatomically informed constraints\n\n');
for model = 1:3
    fprintf(fileID,'Model %d: mean (LH) = %f (%f - %f) | mean (RH) = %f (%f - %f) \n',model,nanmean(time_rDCM_VBinv_AllSubjects{model,1}),nanmin(time_rDCM_VBinv_AllSubjects{model,1}),nanmax(time_rDCM_VBinv_AllSubjects{model,1}),nanmean(time_rDCM_VBinv_AllSubjects{model,2}),nanmin(time_rDCM_VBinv_AllSubjects{model,2}),nanmax(time_rDCM_VBinv_AllSubjects{model,2}));
end

% close the file
fclose(fileID);


% create the results folder
if ( ~exist(fullfile(pre_foldername,aspirin_folder{aspirin_final+1},'DCM_GroupAnalysis',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome']),'dir') )
    mkdir(fullfile(pre_foldername,aspirin_folder{aspirin_final+1},'DCM_GroupAnalysis',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome']))
end

% save the estimated result
if ( ~isempty(results) )
    save(fullfile(pre_foldername,aspirin_folder{aspirin_final+1},'DCM_GroupAnalysis',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome'], 'RunTime_rDCM.mat'), 'results', '-v7.3')
end

end
