function get_rDCM_parameter_estimates_fixed_MainExperiment(model,aspirin,parcellation_type)
% Get the individual parameter estimates from the whole-brain Dynamic Causal 
% Models (DCMs) for the hand movement datasets. Parameter estimates have 
% been computed using regression DCM (rDCM) with an anatomically informed
% A-matrix.
% 
% This function reads the DCM files that have been estimated using
% regression DCM. The function stores the individual parameter estimates
% for the whole-brain effective connectivity patterns.
% 
% Input:
%   model               -- model to analyze
%   aspirin             -- (0) no aspirin and (1) aspirin
%   parcellation_type   -- parcellation scheme: (1) Glasser 2016, (2) AAL
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define the experiment folders
aspirin_folder    = {'wo_Aspirin/','Aspirin/'};
handedness_folder = {'left','right'};

% add the relevant paths and set the foldername
pre_foldername  = FilenameInfo.DataPath;


% define the different names
foldername      = fullfile(pre_foldername, aspirin_folder{aspirin+1});
scale_name      = {'_noScale',''};

% set the parcellation
parcellations     = {'Glasser2016','AAL','Brainnetome2016'};

% define the filename
filename = ['DCM_hand_model' num2str(model) '_rDCM.mat'];


% get the subject list
Subject_List = dir(fullfile(foldername,'1*'));

% which subjects have to be rerun
rerun_subjects = zeros(1,length(Subject_List));


% no scaling of the data
scale = 0;


% specify a indicator vector
if ( aspirin == 0 )
    vector = [1:7 10:length(Subject_List)];
elseif ( aspirin == 1 )
    vector = [1:4 7:length(Subject_List)];
end


% iterate over all subjects
for subject = 1:length(Subject_List)
    
    % subject name
    Subject = Subject_List(subject).name;
    
    % iterate over hand movements
    for handedness = 1:2
    
        % set the directory
        foldername_rDCM = fullfile(foldername, Subject, 'FirstLevel_DCM', ['tapping_' handedness_folder{handedness}], parcellations{parcellation_type}, ['regressionDCM' scale_name{scale+1} '_connectome']);
        
        % get the data
        if ( exist(fullfile(foldername_rDCM,filename),'file') )

            % display subject name
            disp(['Subject: ' Subject ' - found'])

            % load the DCM
            temp           = load(fullfile(foldername_rDCM,filename));
            output         = temp.output;
            
        else

            % display subject name
            if ( any(subject == vector) )
                disp(['Subject: ' Subject ' - missing'])
                rerun_subjects(subject) = 1;
            else
                disp(['Subject: ' Subject ' - excluded'])
            end
            
            % load a dummy result to get network size
            foldername_dummy = fullfile(pre_foldername, 'wo_Aspirin', '111129_elbu', 'FirstLevel_DCM', ['tapping_' handedness_folder{handedness}], parcellations{parcellation_type}, ['regressionDCM' scale_name{scale+1} '_connectome']);
            file_dummy = dir(fullfile(foldername_dummy, 'DCM_hand_model1_rDCM*'));
            temp = load(fullfile(foldername_dummy,file_dummy(1).name));
            
            % define dummy connectivity and driving input matrices
            output.Ep.A = NaN(size(temp.output.Ep.A,1),size(temp.output.Ep.A,2));
            output.Ep.C = NaN(size(temp.output.Ep.C,1),size(temp.output.Ep.C,2));
            
            % define dummy time series
            output.signal.y_source    = NaN(size(temp.output.signal.y_source,1),size(temp.output.signal.y_source,2));
            output.signal.y_pred_rdcm = NaN(size(temp.output.signal.y_pred_rdcm,1),size(temp.output.signal.y_pred_rdcm,2));
            
            % define a dummy array
            output.logF = NaN;
            
        end

        % define empty cells
        if ( subject == 1 && handedness == 1 )
            A_Matrix_AllSubjects    = cell(size(output.Ep.A,1),size(output.Ep.A,2),2);
            C_Matrix_AllSubjects    = cell(size(output.Ep.C,1),size(output.Ep.C,2),2);
            F_AllSubjects           = cell(1,2);
            y_source_AllSubjects  	= cell(1,2);
            y_pred_rdcm_AllSubjects = cell(1,2);
        end

        % asign the a values for the endogenous parameters in each subject
        for int = 1:size(output.Ep.A,1)
            for int2 = 1:size(output.Ep.A,2)
                A_Matrix_AllSubjects{int,int2,handedness} = [A_Matrix_AllSubjects{int,int2,handedness}; output.Ep.A(int,int2)];
            end 
        end

        % asign the a values for the driving parameters in each subject
        for int = 1:size(output.Ep.C,1)
            for int2 = 1:size(output.Ep.C,2)
                C_Matrix_AllSubjects{int,int2,handedness} = [C_Matrix_AllSubjects{int,int2,handedness}; output.Ep.C(int,int2)];
            end 
        end
        
        
        % asign the measured and predicted time series
        y_source_AllSubjects{1,handedness}    = [y_source_AllSubjects{1,handedness}; output.signal.y_source'];
        y_pred_rdcm_AllSubjects{1,handedness} = [y_pred_rdcm_AllSubjects{1,handedness}; output.signal.y_pred_rdcm'];
        
        
        % asign the negative free energy
        F_AllSubjects{1,handedness}    = [F_AllSubjects{1,handedness}; output.logF];
        
        
        % asign the subject name
        results.AllSubjects{subject}        = Subject;
        results_signal.AllSubjects{subject} = Subject;
        
        
        % asign the region names
        if ( subject == 1 && handedness == 1 )
            results.AllRegions        = output.signal.name;
            results_signal.AllRegions = output.signal.name;
        end
    end
end

% asign the results
results.A_Matrix_AllSubjects               = A_Matrix_AllSubjects;
results.C_Matrix_AllSubjects               = C_Matrix_AllSubjects;
results.F_AllSubjects                      = F_AllSubjects;

results_signal.y_source_AllSubjects        = y_source_AllSubjects;
results_signal.y_pred_rdcm_AllSubjects     = y_pred_rdcm_AllSubjects;
results_signal.F_AllSubjects               = F_AllSubjects;


% display which subject numbers to rerun
disp('Re-run subjects:')
disp(find(rerun_subjects))


% create the results folder
if ( ~exist(fullfile(foldername,'DCM_GroupAnalysis',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome']),'dir') )
    mkdir(fullfile(foldername,'DCM_GroupAnalysis',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome']))
end

% save the estimated result
if ( ~isempty(results) )
    save(fullfile(foldername,'DCM_GroupAnalysis',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome'], ['PosteriorParameterEstimates_model' num2str(model) '.mat']), 'results', '-v7.3')
end

% save the estimated result
if ( ~isempty(results_signal) )
    save(fullfile(foldername,'DCM_GroupAnalysis',parcellations{parcellation_type},['regressionDCM' scale_name{scale+1} '_connectome'], ['MeasuredPredictedSignal_model' num2str(model) '.mat']), 'results_signal', '-v7.3')
end

end
