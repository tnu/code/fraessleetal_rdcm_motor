function create_BrainNet_files_MainExperiment(adjacency_matrix,figure_folder,filename_BrainNet)
% Takes an adjacency matrix and creates the node and edge files that are 
% needed by BrainNet to create a volume plot. The folder and filename of 
% the BrainNet file also have to be provided to the function.
% 
% Input:
%   adjacency_matrix    -- matrix encoding the connectivity pattern
%   figure_folder       -- folder where to store files
%   filename_BrainNet   -- filename of node and edge files
% 
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% set the parcellation
parcellations = {'Glasser2016','AAL','Brainnetome2016','Glasser2016_Buckner2011'};


% add the SPM path
addpath(FilenameInfo.SPM_path)

% set the project folder 
foldername = FilenameInfo.DataPath;

% load the brain regions that entered the DCM analyses
files = dir(fullfile(FilenameInfo.LongTermStorage_path,'parcellation',parcellations{3},'all_masks','*nii'));

% load the missing regions
MissingRegions = load(fullfile(foldername,'VoI_coordinates','IBS',parcellations{3},'MissingROIs.mat'));

% get all the regions
region_ind = 1:length(files);

% get the included regions
region_ind_cut = setdiff(region_ind,MissingRegions.allMissing_regions_ind);


% define the results array
MNI_coordinates = cell(1,length(region_ind_cut));

% get mean MNI coordinates
for region = 1:length(region_ind_cut)
    
    % mask name
    region_name = files(region_ind_cut(region)).name;
    
    % get the respective AAL mask
    V = spm_vol(fullfile(FilenameInfo.LongTermStorage_path,'parcellation',parcellations{3},'all_masks',region_name));
    
    % read the volume and the coordinates
    [Y,temp] = spm_read_vols(V);
    
    % get the MNI coordinates of all voxels in the mask
    Y_vector = Y(:);
    MNI_allVoxels = temp(:,Y_vector~=0);
    
    % get the mean MNI coordinate
    MNI_coordinates{region} = round(mean(MNI_allVoxels,2))';
    
end

% create the results folder
if ( ~exist(fullfile(foldername,'VoI_coordinates','IBS',parcellations{3},'BrainNet'),'dir') )
    mkdir(fullfile(foldername,'VoI_coordinates','IBS',parcellations{3},'BrainNet'))

end

% save the MNI coordinates for the AAL parcels
save(fullfile(foldername,'VoI_coordinates','IBS',parcellations{3},'BrainNet','MNI_coordinates.mat'),'MNI_coordinates')


% create the folder
if ( ~exist(figure_folder,'dir') )
    mkdir(figure_folder)
end


% convert into the BrainNet format
if ( exist(fullfile(figure_folder, [filename_BrainNet '.node']),'file') )
    delete(fullfile(figure_folder, [filename_BrainNet '.node']))
end
fileID = fopen(fullfile(figure_folder, [filename_BrainNet '.node']),'w');
for region = 1:size(adjacency_matrix,1)
    fprintf(fileID,[num2str(MNI_coordinates{region}(1)) '\t' num2str(MNI_coordinates{region}(2)) '\t' num2str(MNI_coordinates{region}(3)) '\t' '1' '\t' '1\n']);
end
fclose(fileID);



% create a matrix with edge weights
% NOTE: BrainNet codes directionality different to DCM (rows -> columns)
if ( exist(fullfile(figure_folder, [filename_BrainNet '.edge']),'file') )
    delete(fullfile(figure_folder, [filename_BrainNet '.edge']))
end
fileID = fopen(fullfile(figure_folder, [filename_BrainNet '.edge']),'w');
for region = 1:size(adjacency_matrix,2)
    connections = adjacency_matrix(:,region)';
    connections(region) = 0;
    connections_string  = [];
    for int = 1:size(adjacency_matrix,2)-1
        connections_string = [connections_string [num2str(connections(1,int)) '\t']];
    end
    connections_string = [connections_string [num2str(connections(1,int+1)) '\n']];
    fprintf(fileID,connections_string);    
end
fclose(fileID);


% create a matrix excitatory and inhibitory connections
% NOTE: BrainNet codes directionality different to DCM (rows -> columns)
if ( exist(fullfile(figure_folder, [filename_BrainNet '_signed.edge']),'file') )
    delete(fullfile(figure_folder, [filename_BrainNet '_signed.edge']))
end
fileID = fopen(fullfile(figure_folder, [filename_BrainNet '_signed.edge']),'w');
for region = 1:size(adjacency_matrix,2)
    connections = adjacency_matrix(:,region)';
    connections(region) = 0;
    connections         = sign(connections);
    connections_string  = [];
    for int = 1:size(adjacency_matrix,2)-1
        connections_string = [connections_string [num2str(connections(1,int)) '\t']];
    end
    connections_string = [connections_string [num2str(connections(1,int+1)) '\n']];
    fprintf(fileID,connections_string);    
end
fclose(fileID);

end