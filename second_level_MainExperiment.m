function second_level_MainExperiment(handedness,aspirin)
% Specify the group level General Linear Model (GLM) for the hand movement
% paradigm of the stroke / aspirin dataset.
% 
% This function reads a Matalb Batch and the respective first level contrast 
% images to specify the GLM for group level analysis of BOLD activation 
% correlated with left- or right-hand movements. The GLM is then estimated 
% and the respective contrasts are specified.
% 
% Input:
% 	handedness         	-- left- or right-hand movement dataset
%   aspirin             -- subjects (0) without or (1) with aspirin, ([]) both groups
%
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2019
% Copyright 2019 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% get the path
m_path = mfilename('fullpath');
m_path = m_path(1:find(m_path=='/',1,'last'));

try
    load(fullfile(m_path,'ConfigFile.mat'))
catch err
    disp('Need to specify FilenameInfo containing paths!')
    rethrow(err)
end


% define important folders
aspirin_folder = {'wo_Aspirin/','Aspirin/'};
handedness_folder = {'left','right'};


% set the default
if ( isempty(handedness) )
    handedness_analyze = [1 2];
else
    handedness_analyze = handedness;
end

% set the default
if ( isempty(aspirin) )
    aspirin_analyze = [0 1];
else
    aspirin_analyze = aspirin;
end


% iterate over different conditions
for handedness = handedness_analyze
    
    % specify some variables
    contrast_name   = {[handedness_folder{handedness} 'hand > baseline']};
    RunBatch        = 1;
    
    % in case no contrast has been defined
    contrast_number = 1:length(contrast_name);
    
    % go through the different contrasts
    for contrast = contrast_number
    
        % set the SPM path
        addpath(genpath(FilenameInfo.SPM_path));
        folder_results 	 = FilenameInfo.DataPath;
        batch_foldername = FilenameInfo.Batch_path;
        
        % load the example Matlab Batch
        load(fullfile(batch_foldername, 'Batch_SecondLevelModelSpecifications.mat'))
        
        % display what is done at the moment
        disp(['Group Level - Model specification - ' contrast_name{contrast}])
        
        % modify the working directory where the SPM.mat should be saved
        matlabbatch{1}.spm.stats.factorial_design.dir = [];

        % get the correct folder name
        folder = fullfile(folder_results, 'SecondLevel', 'IBS', 'one_sample_ttest', contrast_name{contrast});

        % create the folder if it does not exist
        if ( ~exist(folder,'dir') )
            mkdir(folder)
        end

        % assign the new filename
        matlabbatch{1}.spm.stats.factorial_design.dir = {folder};
        
        
        % delete the filenames of the scans and load the new filenames
        matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = [];
        
        
        for aspirin = aspirin_analyze
            
            % initialize spm
            spm('Defaults','fMRI');
            spm_jobman('initcfg');
            
            
            % aspirin specific folders
            folder_results_aspirin    = [folder_results aspirin_folder{aspirin+1}];


            % subject list
            Subject_List = dir(fullfile(folder_results_aspirin, '1*'));
            
            
            % select the subjects to be analyze
            if ( aspirin == 0 )
                subjects_analyze = [1:7 10:14];
            else
                subjects_analyze = [1:4 7:15];
            end
            
            
            % display the number of subjects in the restricted sample
            disp(['# of subjects (total): ' num2str(length(Subject_List)) ' - (used): ' num2str(length(subjects_analyze))])
            
            
            % assign the new file name to the matlabbatch
            for s = subjects_analyze

                % get the subject name
                Subject = Subject_List(s).name;

                if ( exist(fullfile(folder_results_aspirin, Subject, 'FirstLevel', 'IBS', ['tapping_' handedness_folder{handedness}], ['con_000' num2str(contrast) '.nii']),'file') )

                    % get the correct filenames
                    files = dir(fullfile(folder_results_aspirin, Subject, 'FirstLevel', 'IBS', ['tapping_' handedness_folder{handedness}], ['con_000' num2str(contrast) '*']));

                    % asign the filenames to matlabbatch
                    matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = [matlabbatch{1}.spm.stats.factorial_design.des.t1.scans; {fullfile(folder_results_aspirin, Subject, 'FirstLevel', 'IBS', ['tapping_' handedness_folder{handedness}], [files(1).name ',1'])}];

                end
            end
        end


        %% estimate the first level GLM

        % specify the estimation part
        matlabbatch{2}.spm.stats.fmri_est.spmmat = cellstr(fullfile(folder,'SPM.mat'));


        %% do the inference

        % specify contrasts
        matlabbatch{3}.spm.stats.con.spmmat = cellstr(fullfile(folder,'SPM.mat'));
        matlabbatch{3}.spm.stats.con.consess{1}.tcon.name = contrast_name{contrast};
        matlabbatch{3}.spm.stats.con.consess{1}.tcon.weights = 1;


        %% save and run the created matlabbatch

        % save the batch
        save(fullfile(folder, 'secondlevel.mat'), 'matlabbatch');

        % check whether SPM already exists
        if ( exist(fullfile(folder,'SPM.mat'),'file') )
            delete(fullfile(folder,'SPM.mat'))
        end

        % run the batch
        if ( RunBatch == 1 )
            spm_jobman('run',matlabbatch)
        end

        % clear the matlabbatch
        clear matlabbatch

    end
end

end
