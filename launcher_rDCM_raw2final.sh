#! /bin/bash

set -e

# Load the matlab module
module load matlab


# IDs for the preprocessing
IDS_woAspirin="1 2 3 4 5 6 7 8 9 10 11 12 13 14"
IDS_Aspirin="1 2 3 4 5 6 7 8 9 10 11 12 13 14 15"


# Run the first level (no Asprin group)
for ID in $IDS_woAspirin
do
	bsub -W 2:00 -J "job_firstlevel_woAspirin_$ID" -o log_firstlevel_woAspirin_"$ID" matlab -singleCompThread -r "first_level_MainExperiment($ID,[],0)"
done


# Run the first level (Asprin group)
for ID in $IDS_Aspirin
do
	bsub -W 2:00 -J "job_firstlevel_Aspirin_$ID" -o log_firstlevel_Aspirin_"$ID" matlab -singleCompThread -r "first_level_MainExperiment($ID,[],1)"
done


# Extract VOI signal time series from the pre-defined regions of interest
for ID in $IDS_woAspirin
do
	for hand in 1 2
	do
		bsub -W 2:00 -J "job_timeseries_woAspirin_$ID" -w 'ended("job_firstlevel_woAspirin*")' -o log_timeseries_woAspirin_"$ID" matlab -singleCompThread -r "extract_VOI_timeseries_MainExperiment($ID,$hand,0,3)"
	done
done


# Extract VOI signal time series from the pre-defined regions of interest
for ID in $IDS_Aspirin
do
	for hand in 1 2
	do
		bsub -W 2:00 -J "job_timeseries_Aspirin_$ID" -w 'ended("job_firstlevel_Aspirin*")' -o log_timeseries_Aspirin_"$ID" matlab -singleCompThread -r "extract_VOI_timeseries_MainExperiment($ID,$hand,1,3)"
	done
done



# IDs for the main analyses (subjects excluded because of non-compliance with the task, poor activation patterns, wrong scanner settings, etc.)
IDS_woAspirin_rDCM="1 2 3 4 5 6 7 10 11 12 13 14"
IDS_Aspirin_rDCM="1 2 3 4 7 8 9 10 11 12 13 14 15"


# Run the second level once the first level has finished
for hand in 1 2
do
	bsub -W 2:00 -w 'ended("job_firstlevel*")' -o log_secondlevel matlab -singleCompThread -r "second_level_MainExperiment($hand,[])"
done


# Get the center coordinates of the regions of interest (parcels)
bsub -W 2:00 -J "job_VOIcoord" -w 'ended("job_timeseries*")' -o log_VOIcoordinates matlab -singleCompThread -r "get_VOI_coordinates_MainExperiment(3)"


# Get regions that were missing in at least one of the subjects and exclude those VoIs in both hemispheres
bsub -W 2:00 -J "job_missingVOI" -w 'ended("job_VOIcoord")' -o log_missingVOIs matlab -singleCompThread -r "check_indvidual_coordinates_MainExperiment(3)"



# Run the whole-brain effective connectivity analysis with rDCM (no Aspirin group)
for ID in $IDS_woAspirin_rDCM
do
	for model in 1 2 3
	do
		for hand in 1 2
		do
			bsub -W 2:00 -R "rusage[mem=7000]" -J "job_rDCM_woAspirin_$ID,$model,$hand" -w 'ended("job_missingVOI")' -o log_rDCM_woAspirin_"$ID" matlab -singleCompThread -r "estimate_rDCM_fixed_MainExperiment($ID,$model,0,$hand,3)"
		done
	done
done



# Run the whole-brain effective connectivity analysis with rDCM (Aspirin group)
for ID in $IDS_Aspirin_rDCM
do      
	for model in 1 2 3
	do
		for hand in 1 2
		do
			bsub -W 2:00 -R "rusage[mem=7000]" -J "job_rDCM_Aspirin_$ID,$model,$hand" -w 'ended("job_missingVOI")' -o log_rDCM_Aspirin_"$ID" matlab -singleCompThread -r "estimate_rDCM_fixed_MainExperiment($ID,$model,1,$hand,3)"
		done
	done
done


# Collect the estimated whole-brain effective connectivity estimates
for model in 1 2 3
do
	for aspirin in 0 1
	do
		bsub -W 2:00 -R "rusage[mem=7000]" -J "job_rDCM_collect" -w 'ended("job_rDCM_*")' -o log_rDCM_collect_model"$model"_"$aspirin" matlab -singleCompThread -r "get_rDCM_parameter_estimates_fixed_MainExperiment($model,$aspirin,3)"
	done
done



# Evaluate functional connectivity among BOLD signal time series (no Aspirin group)
for hand in 1 2
do
	bsub -W 24:00 -J "job_FC" -w 'ended("job_missingVOI")' -o log_FC_woAspirin matlab -singleCompThread -r "estimate_FC_MainExperiment([$IDS_woAspirin_rDCM],0,$hand,3,1)"
done


# Evaluate functional connectivity among BOLD signal time series (Aspirin group)
for hand in 1 2
do
	bsub -W 24:00 -J "job_FC" -w 'ended("job_missingVOI")' -o log_FC_Aspirin matlab -singleCompThread -r "estimate_FC_MainExperiment([$IDS_Aspirin_rDCM],1,$hand,3,1)"
done



# Evaluate summary statistics of whole-brain effective connectivity estimates and create plots for left- and right-hand movements in all included subjects
for hand in 1 2
do
	bsub -W 2:00 -J "job_rDCM_LR" -w 'ended("job_rDCM_collect") && ended("job_FC")' -o log_rDCM_statistics matlab -singleCompThread -r "statistics_rDCM_parameter_estimates_fixed_MainExperiment(1,[],$hand,3,1,1,1)"
done
 
 
# Compare left- and right-hand movements in all included subjects
bsub -W 2:00 -J "job_rDCM_LR_diff" -w 'ended("job_rDCM_collect")' -o log_rDCM_LRdiff matlab -singleCompThread -r "compare_LR_rDCM_fixed_MainExperiment(1,[],3,2,1)"


# Compare aspirin and no Aspirin group (merely to show that we can combine the groups since there are no differences)
bsub -W 2:00 -J "job_rDCM_AwA_diff" -w 'ended("job_rDCM_collect")' -o log_rDCM_AwAdiff matlab -singleCompThread -r "compare_aspirin_rDCM_fixed_MainExperiment(1,3,1)"


# Model comparison in all included subjects
bsub -W 2:00 -J "job_rDCM_BMS" -w 'ended("job_rDCM_collect")' -o log_rDCM_BMS matlab -singleCompThread -r "BMS_rDCM_fixed_MainExperiment([],[],3)"


# Get the run-times for the rDCM analysis
bsub -W 2:00 -J "job_rDCM_times" -w 'ended("job_rDCM_collect")' -o log_rDCM_runtime matlab -singleCompThread -r "get_rDCM_time_MainExperiment([],3)"


# Compare left- and right-hand movements for functional connectivity in all included subjects
bsub -W 2:00 -J "job_FC_LR_diff" -w 'ended("job_FC")' -o log_FC_LRdiff matlab -singleCompThread -r "compare_LR_FC_MainExperiment([],1,3,2,1)"



# Run the whole-brain effective connectivity analysis with sparse rDCM (no Aspirin group)
for ID in $IDS_woAspirin_rDCM
do
	for hand in 1 2
	do
		for p_ind in {1..12}
		do
			bsub -W 48:00 -R "rusage[mem=14000]" -J "job_srDCM_woAspirin_$ID,$hand,$p_ind" -w 'ended("job_missingVOI")' -o log_srDCM_woAspirin_"$ID" matlab -singleCompThread -r "estimate_srDCM_MainExperiment($ID,1,0,$hand,3,0,$p_ind,0)"
		done
	done
done


# Run the whole-brain effective connectivity analysis with sparse rDCM (Aspirin group)
for ID in $IDS_Aspirin_rDCM
do
	for hand in 1 2
	do
		for p_ind in {1..12}
		do
			bsub -W 48:00 -R "rusage[mem=14000]" -J "job_srDCM_woAspirin_$ID,$hand,$p_ind" -w 'ended("job_missingVOI")' -o log_srDCM_Aspirin_"$ID" matlab -singleCompThread -r "estimate_srDCM_MainExperiment($ID,1,1,$hand,3,0,$p_ind,0)"
		done
	done
done


# Collect the sparse rDCM results for the different p0 values (no Aspirin group)
for hand in 1 2
do
	bsub -W 2:00 -R "rusage[mem=7000]" -J "job_srDCM_summarize" -w 'ended("job_srDCM_*")' -o log_srDCM_summarize_woAspirin matlab -singleCompThread -r "summarize_srDCM_parameter_estimates_MainExperiment([$IDS_woAspirin_rDCM],1,0,$hand,0,3,1)"
done


# Collect the sparse rDCM results for the different p0 values (Aspirin group)
for hand in 1 2
do
	bsub -W 2:00 -R "rusage[mem=7000]" -J "job_srDCM_summarize" -w 'ended("job_srDCM_*")' -o log_srDCM_summarize_Aspirin matlab -singleCompThread -r "summarize_srDCM_parameter_estimates_MainExperiment([$IDS_Aspirin_rDCM],1,1,$hand,0,3,1)"
done


# Collect the estimated whole-brain sparse effective connectivity estimates
for aspirin in 0 1
do
	bsub -W 2:00 -R "rusage[mem=7000]" -J "job_srDCM_collect" -w 'ended("job_srDCM_summarize")' -o log_srDCM_collect_"$aspirin" matlab -singleCompThread -r "get_srDCM_parameter_estimates_MainExperiment(1,$aspirin,0,3)"
done


# Create group-level plots for left- and right-hand movements and compute graph-theoretical measures (sparse rDCM) in all included subjects
for hand in 1 2
do
	bsub -W 2:00 -J "job_srDCM_LR_$hand" -w 'ended("job_srDCM_collect")' -o log_srDCM_statistics matlab -singleCompThread -r "statistics_srDCM_parameter_estimates_MainExperiment(1,[],$hand,3,0,1,[],1)"
done


# Compare left- and right-hand movements for sparse rDCM in all included subjects
bsub -W 2:00 -J "job_srDCM_LR_diff" -w 'ended("job_srDCM_collect")' -o log_srDCM_LRdiff matlab -singleCompThread -r "compare_LR_srDCM_MainExperiment(1,[],3,0,2,1)"


# Compare aspirin and no Aspirin group (merely to show that we can combine the groups since there are no differences)
bsub -W 2:00 -J "job_srDCM_AwA_diff" -w 'ended("job_srDCM_collect")' -o log_srDCM_AwAdiff matlab -singleCompThread -r "compare_aspirin_srDCM_MainExperiment(1,3,0,1)"


# Get the run-times for the sparse rDCM analysis
bsub -W 2:00 -J "job_srDCM_times" -w 'ended("job_srDCM_collect")' -o log_srDCM_times matlab -singleCompThread -r "get_srDCM_time_MainExperiment([],0,3,0)"


# Model comparison (rDCM vs srDCM) in all included subjects
bsub -W 2:00 -J "job_srDCM_BMS" -w 'ended("job_srDCM_collect")' -o log_srDCM_BMS matlab -singleCompThread -r "BMS_srDCM_MainExperiment([],[],0,3)"



# Create a volume out of the graph-theoretical measures (for display)
for hand in 1 2
do
	bsub -W 4:00 -J "job_srDCM_GTvol" -w 'ended("job_srDCM_LR*")' -o log_srDCM_GTvol matlab -singleCompThread -r "create_volume_graph_theoretical_measures_MainExperiment(1,$hand,2,3,0)"
done


# Create a volume out of the graph-theoretical measures for the functional connectivity (for display)
for hand in 1 2
do
	bsub -W 4:00 -J "job_FC_GTvol" -w 'ended("job_FC")' -o log_FC_GTvol matlab -singleCompThread -r "create_volume_graph_theoretical_measures_FC_MainExperiment($hand,3)"
done

