function [ options ] = vbsv_graphics_options( )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

options.model = 'cmc';

options.dcm.nSource = 2;

options.top.Marker = {'h', 'p', 'd', 'v'};
options.top.Color = [...
    0 0 0; ...
    0.2 0.2 0.2; ...
    0.4 0.4 0.4; ...
    0.6 0.6 0.6];
options.top.MarkerSize = [12 12 8 8];
options.default.Marker = 'o';
options.default.Color = [0.8 0 0];
options.default.MarkerSize = 8;
options.normal.Marker = 'o';
options.normal.Color = [0.8 0.8 0.8]; 

options.F.default.FaceColor = [1 0 0]; 
options.F.notConverged.FaceColor = [0.4 0.4 0.4];
options.F.all.FaceColor = [0.7 0.7 0.7];
options.F.KL.LineWidth = 2;
options.F.KL.Color = [0.392, 0.475, 0.635];
options.F.A.LineWidth = 2;
options.F.A.Color = [0.392, 0.475, 0.635];

options.vE.default.FaceColor = [1 0 0]; 
options.vE.notConverged.FaceColor = [0.7 0.7 0.7];
options.vE.all.FaceColor = [0 0 0];
options.vE.F.LineWidth = 2;
options.vE.F.Color = [0.392, 0.475, 0.635];

options.Ep.Marker.top.size = [12, 12, 8, 8];
options.Ep.Marker.default.size = [6];
options.Ep.Marker.normal.size = [3];

options.Ep.Marker.top.FaceColor = options.top.Color;
options.Ep.Marker.default.FaceColor = options.default.Color;
options.Ep.Marker.normal.FaceColor = options.normal.Color; 

options.Ep.Marker.top.EdgeColor = 'none';
options.Ep.Marker.default.EdgeColor = 'none';
options.Ep.Marker.normal.EdgeColor = 'none';

options.Ep.Bar.FaceColor = 0.9 * [1 1 1];
options.Ep.Bar.EdgeColor = 'none';

options.input.top.Marker = options.top.Marker;
options.input.top.LineWidth = 2.5;
options.input.top.MarkerSize = [11 11 8 8];
options.input.top.Color = options.top.Color;
options.input.default.Marker = {'o'};
options.input.default.MarkerSize = 8;
options.input.default.LineWidth = 2.5;
options.input.default.Color = options.default.Color;

end