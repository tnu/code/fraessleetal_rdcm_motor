function get_input_regressor_MainExperiment(onCluster)
% Obtains the input regressor from the SPM.mat and stores the result in a
% seperate file.
% 
% Input:
%   onCluster           -- operation system: (1) Euler, (0) local machine
%   
% Output:
% 

% ----------------------------------------------------------------------
% 
% stefan_fraessle@gmx.de
%
% Author: Stefan Fraessle, TNU, UZH & ETHZ - 2018
% Copyright 2018 by Stefan Fraessle <stefan_fraessle@gmx.de>
%
% Licensed under GNU General Public License 3.0 or later.
% Some rights reserved. See COPYING, AUTHORS.
% 
% ----------------------------------------------------------------------


% define the experiment folders
aspirin_folder    = {'wo_Aspirin/','Aspirin/'};
handedness_folder = {'left','right'};


% on Euler or not? add the relevant paths and set the foldername
if ( onCluster == 1 )
    addpath '/cluster/home/stefanf/matlab/spm8v/';
    pre_foldername  = '/cluster/scratch/stefanf/rDCM_Stroke/';
else
    addpath('/Users/stefanf/Documents/SPM/spm8v/')
    pre_foldername = '/Users/stefanf/Documents/ETH Zurich/Projects/rDCM_Stroke/';
end


% specify the results array
Design_AllSubjects = cell(2,15,2);


% iterate over aspirin condition
for aspirin = [0 1]

    % define the different names
    foldername        = [pre_foldername aspirin_folder{aspirin+1}];
    
    % get the subject list
    Subject_List = [dir(fullfile(foldername,'1*')); dir(fullfile(foldername,'2*')); dir(fullfile(foldername,'3*'))];
    
    % iterate over subjects
    for subject = 1:length(Subject_List)

        % subject name
        Subject = Subject_List(subject).name;

        % display subject name
        disp(['Subject: ' Subject])

        % iterate over handedness
        for handedness = [1 2]

            % load the SPM
            load(fullfile(foldername, Subject, 'FirstLevel', 'IBS', ['tapping_' handedness_folder{handedness}], 'SPM.mat'))

            % store the regressor
            Design_AllSubjects{aspirin+1,subject,handedness} = SPM.xX.X(:,1);
            
        end
    end
end


% create the directory
if ( ~exist(fullfile(pre_foldername, 'ExpDesign'),'dir') )
    mkdir(fullfile(pre_foldername, 'ExpDesign'))
end

% save the estimated results
if ( ~isempty(Design_AllSubjects) )
    save(fullfile(pre_foldername, 'ExpDesign','Design_AllSubjects.mat'),'Design_AllSubjects')
end

end
